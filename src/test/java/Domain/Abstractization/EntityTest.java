package Domain.Abstractization;


import org.junit.jupiter.api.*;

class EntityTest {

    @Test
    void getId() {
        Entity<Long> testEntity=new Entity<>();
        testEntity.setId(5L);
        Assertions.assertEquals(5L,testEntity.getId());
    }

    @Test
    void setId() {
        Entity<Long> testEntity=new Entity<>();
        testEntity.setId(5L);
        Assertions.assertEquals(5L,testEntity.getId());
    }

    @Test
    void testEquals() {
        Entity<Long> testEntity=new Entity<>();
        testEntity.setId(5L);
        Entity<Long> entity = new Entity<>();
        entity.setId(5L);
        Assertions.assertEquals(testEntity,entity);
    }
}