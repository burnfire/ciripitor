package Domain.Abstractization;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PairTest {

    Pair<Long> longPair = new Pair<>(5L,4L);
    @Test
    @Order(1)
    void getLeft() {
        assertEquals(5L,longPair.getLeft());
    }

    @Test
    @Order(2)
    void getRight() {
        assertEquals(4L,longPair.getRight());
    }
}