package Domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class MessageTest {

    Message msg = new Message(10L,"Da",1L,5L, LocalDateTime.now());
    @Test
    void getMessage() {
        assertEquals("Da",msg.getMessage());
    }

    @Test
    void getFrom() {
        assertEquals(1L,msg.getFrom());
    }

    @Test
    void getTo() {
        assertTrue(msg.getTo().contains(5L));
    }

    @Test
    void getDateTime() {
        assertNotNull(msg.getDateTime());
    }
}