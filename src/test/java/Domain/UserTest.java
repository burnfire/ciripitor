package Domain;

import org.junit.Test;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.TestMethodOrder;

import static org.junit.Assert.*;

@TestMethodOrder(MethodOrderer.class)
public class UserTest {

    User user = new User(5L,"Stefanescu","Petru","burnfire","123");
    @Test
    public void getFirstName() {
        assertEquals("Stefanescu",user.getFirstName());
    }

    @Test
    public void setFirstName() {
        user.setFirstName("Petrescu");
        assertEquals("Petrescu",user.getFirstName());
    }

    @Test
    public void getLastName() {
        assertEquals("Petru",user.getLastName());
    }

    @Test
    public void setLastName() {
        user.setLastName("Petrov");
        assertEquals("Petrov",user.getLastName());
    }

    @Test
    public void getNickname() {
        assertEquals("burnfire",user.getNickname());
    }

    @Test
    public void setNickname() {
        user.setNickname("burnfire123");
        assertEquals("burnfire123",user.getNickname());
    }

    @Test
    public void getPasswd() {
        assertEquals("123",user.getPasswd());
    }
    @Test
    public void setPasswd() {
        user.setPasswd("1234");
        assertEquals("1234",user.getPasswd());
    }
}