package Domain;

import org.junit.Test;

import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class ReplyMessageTest {

    @Test
    public void getReplyTo() {
        assertEquals(10L,(long)new ReplyMessage(10L,12L,"Raspuns",1L,2L, LocalDateTime.now()).getReplyTo());
    }
}