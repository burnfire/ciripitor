package Domain;

import Domain.Friendships.Friendship;
import Domain.Friendships.FriendshipStatus;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FriendshipTest {

    Friendship friendship = new Friendship(1L,2L, LocalDateTime.now(), FriendshipStatus.PENDING);
    @Test
    @Order(1)
    void acceptFriendship() {
        friendship.acceptFriendship();
    }

    @Test
    @Order(2)
    void isAccepted() {
        friendship.setStatus(FriendshipStatus.PENDING);
        friendship.acceptFriendship();
        assertTrue(friendship.isAccepted());
    }

    @Test
    @Order(3)
    void getDateTime() {
        assertNotNull(friendship.getDateTime());
    }

    @Test
    @Order(4)
    void setDateTime() {
        LocalDateTime date = LocalDateTime.now();
        friendship.setDateTime(date);
        assertEquals(date,friendship.getDateTime());
    }
}