package Repository.Loaders;

import java.sql.SQLException;
import java.util.List;

public interface Loader<E>{
    /**
     * gets all entities from the persistent storage
     * @return a {@link List} of all entities
     */
    List<E> getAll() throws SQLException;
}
