package Repository.Loaders.DBLoader;

import Domain.Friendships.Friendship;
import Domain.Friendships.FriendshipStatus;
import Repository.Loaders.Pager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FriendshipDBLoader extends AbstractDBLoader<Friendship> implements Pager<Friendship> {
    public FriendshipDBLoader(Connection connection) {
        super(connection);
    }

    /**
     * gets all entities from the persistent storage
     *
     * @return a {@link List} of all entities
     */
    @Override
    public List<Friendship> getAll() throws SQLException {
        List<Friendship> all = new ArrayList<>();
        String query = "SELECT sender,receiver,datetime,status FROM friendships";
        return getFriendships(query);
    }

    /**
     * gets all entities from a specific page
     *
     * @param pageSize   number of entities per page
     * @param pageNumber page's number
     * @return a {@link List < E >} of entities contained in the desired page
     */
    @Override
    public List<Friendship> getAll(int pageSize, int pageNumber) throws SQLException {
        int startFrom = (pageNumber-1)*pageSize;
        String query = String.format("SELECT sender,receiver,datetime,status FROM friendships OFFSET %d ROWS FETCH FIRST %d ROWS ONLY",startFrom,pageSize);
        return getFriendships(query);
    }

    private List<Friendship> getFriendships(String query) throws SQLException {
        List<Friendship> all = new ArrayList<>();
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            all.add(new Friendship(
                    resultSet.getLong(1),
                    resultSet.getLong(2),
                    resultSet.getTimestamp(3).toLocalDateTime(),
                    (resultSet.getInt(4)==0? FriendshipStatus.PENDING: FriendshipStatus.ACCEPTED)));
        }
        return all;
    }
}
