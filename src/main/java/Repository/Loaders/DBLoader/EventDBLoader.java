package Repository.Loaders.DBLoader;

import Domain.Events.Event;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventDBLoader extends AbstractDBLoader<Event>{
    public EventDBLoader(Connection connection) {
        super(connection);
    }

    /**
     * gets all entities from the persistent storage
     *
     * @return a {@link List} of all entities
     */
    @Override
    public List<Event> getAll() throws SQLException {
        List<Event> all = new ArrayList<>();
        String query="SELECT id,title,description,startdate,enddate,creator_id FROM Events";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            Event event = new Event(
                    resultSet.getLong(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getTimestamp(4).toLocalDateTime(),
                    resultSet.getTimestamp(5).toLocalDateTime(),
                    resultSet.getLong(6),null
            );
            event.setAttendees(getAttendees(resultSet.getLong(1)));
            all.add(event);

        }
        return all;
    }

    private List<Long> getAttendees(long aLong) throws SQLException {
        String query = String.format("SELECT user_id FROM participations WHERE event_id=%d",aLong);
        List<Long> userIds=new ArrayList<>();
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            userIds.add(resultSet.getLong(1));
        }
        return userIds;
    }
}
