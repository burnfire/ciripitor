package Repository.Loaders.DBLoader;

import Repository.Loaders.Loader;

import java.sql.Connection;

public abstract class AbstractDBLoader<E> implements Loader<E> {
    Connection connection;

    public AbstractDBLoader(Connection connection) {
        this.connection = connection;
    }
}
