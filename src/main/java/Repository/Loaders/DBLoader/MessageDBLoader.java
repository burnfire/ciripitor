package Repository.Loaders.DBLoader;

import Domain.Message;
import Domain.ReplyMessage;
import Repository.Loaders.Loader;
import Repository.Loaders.Pager;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MessageDBLoader extends AbstractDBLoader<Message> implements Pager<Message> {
    public MessageDBLoader(Connection connection) {
        super(connection);
    }

    /**
     * gets all entities from the persistent storage
     *
     * @return a {@link List} of all entities
     */
    @Override
    public List<Message> getAll() throws SQLException {
        String query="SELECT id,sender,message,datetime,replyto FROM messages";
        return getMessages(query);
    }

    private List<Long> getReceivers(Long id) throws SQLException {
        List<Long> all = new ArrayList<>();
        String query=String.format("SELECT receiver_id FROM receivers WHERE message_id=%d",id);
        ResultSet resultSet=connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            all.add(resultSet.getLong(1));
        }
        return all;
    }

    /**
     * gets all entities from a specific page
     *
     * @param pageSize   number of entities per page
     * @param pageNumber page's number
     * @return a {@link List < E >} of entities contained in the desired page
     */
    @Override
    public List<Message> getAll(int pageSize, int pageNumber) throws SQLException {
        int startFrom = (pageNumber-1)*pageSize;
        String query=String.format("SELECT id,sender,message,datetime,replyto FROM messages OFFSET %d ROWS FETCH FIRST %d ROWS ONLY",startFrom,pageSize);
        return getMessages(query);
    }

    private List<Message> getMessages(String query) throws SQLException {
        List<Message> all = new ArrayList<>();
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            Message msg;
            if(resultSet.getLong(5)==0)
                msg = new Message(resultSet.getString(3),resultSet.getLong(2), getReceivers(resultSet.getLong(1)),resultSet.getTimestamp(4).toLocalDateTime());
            else
                msg = new ReplyMessage(resultSet.getString(3), resultSet.getLong(2), getReceivers(resultSet.getLong(1)), resultSet.getTimestamp(4).toLocalDateTime(), resultSet.getLong(5));
            msg.setId(resultSet.getLong(1));
            all.add(msg);
        }
        return all;
    }
}
