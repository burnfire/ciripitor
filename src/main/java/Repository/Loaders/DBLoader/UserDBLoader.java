package Repository.Loaders.DBLoader;

import Domain.User;
import Utilities.Encryption;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDBLoader extends AbstractDBLoader<User> implements Encryption {
    public UserDBLoader(Connection connection) {
        super(connection);
    }

    /**
     * gets all entities from the persistent storage
     *
     * @return a {@link List} of all entities
     */
    @Override
    public List<User> getAll() throws SQLException {
        List<User> all = new ArrayList<>();
        String query = "SELECT users.id,firstname,lastname,nickname,password,email,eventsnotifications FROM users INNER JOIN user_settings ON users.id = user_settings.id";
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            User user = new User(
                    resultSet.getLong(1),
                    decryptString(resultSet.getString(2)),
                    decryptString(resultSet.getString(3)),
                    decryptString(resultSet.getString(4)),
                    decryptString(resultSet.getString(5)),
                    decryptString(resultSet.getString(6))
            );
            User.UserSettings userSettings = new User.UserSettings(resultSet.getBoolean(7));
            user.setUserSettings(userSettings);
            all.add(user);
        }
        return all;
    }
}
