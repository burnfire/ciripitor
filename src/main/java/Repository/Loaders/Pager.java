package Repository.Loaders;

import java.sql.SQLException;
import java.util.List;

public interface Pager<E> {
    /**
     * gets all entities from a specific page
     *
     * @param pageSize   number of entities per page
     * @param pageNumber page's number
     * @return a {@link List < E >} of entities contained in the desired page
     */
    List<E> getAll(int pageSize, int pageNumber) throws SQLException;
}
