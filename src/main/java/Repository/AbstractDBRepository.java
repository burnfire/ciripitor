package Repository;

import Domain.Abstractization.Entity;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Validators.Validator;
import Repository.Deleters.DBDeleters.AbstractDBDeleter;
import Repository.Inserters.DBInserters.AbstractDBInserter;
import Repository.Loaders.DBLoader.AbstractDBLoader;
import Repository.Searchers.DBSearcher.AbstractDBSearcher;
import Repository.Updaters.DBUpdaters.AbstractDBUpdater;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class AbstractDBRepository<K,E extends Entity<K>> implements Repository<K,E>{
    Validator<E> validator;
    Connection connection;
    AbstractDBSearcher<K,E> entitySearcher;
    AbstractDBLoader<E> entityLoader;
    AbstractDBInserter<E> entityInserter;
    AbstractDBUpdater<E> entityUpdater;
    AbstractDBDeleter<K> entityDeleter;
    public AbstractDBRepository(Validator<E> validator, Connection connection,AbstractDBSearcher<K,E> entitySearcher, AbstractDBLoader<E> entityLoader, AbstractDBInserter<E> entityInserter, AbstractDBUpdater<E> entityUpdater, AbstractDBDeleter<K> entityDeleter) {
        this.validator = validator;
        this.connection=connection;
        this.entitySearcher = entitySearcher;
        this.entityLoader = entityLoader;
        this.entityInserter=entityInserter;
        this.entityUpdater=entityUpdater;
        this.entityDeleter=entityDeleter;
    }

    /**
     * Checks if an entity exists
     *
     * @param key - entity's key
     * @return true if the entity exists, false otherwise
     * @throws SQLException if the DB connection went wrong
     */
    @Override
    public boolean exists(K key) throws SQLException,IllegalArgumentException {
        if(key==null)
            throw new IllegalArgumentException("Null key!");
        return entitySearcher.search(key)!=null;
    }

    /**
     * finds an element
     *
     * @param key element's key
     * @return the element if it's found, null otherwise
     * @throws SQLException if DB connection went wrong
     */
    @Override
    public E find(K key) throws SQLException, IllegalArgumentException {
        if(key==null)
            throw new IllegalArgumentException("Null key!");
        return entitySearcher.search(key);
    }

    /**
     * gets all the elements in the repository
     *
     * @return a {@link List} containing all the elements
     * @throws SQLException if DB connection went wrong
     */
    @Override
    public List<E> getAll() throws SQLException {
        return entityLoader.getAll();
    }

    /**
     * adds an entity if it doesn't already exist
     *
     * @param entity the entity to add
     * @throws RepoError                if the entity already exists
     * @throws IllegalArgumentException if entity is null
     * @throws SQLException             if the database communication doesn't work
     * @throws DomainError              if the entity to be added can't be validated
     */
    @Override
    public void add(E entity) throws RepoError, IllegalArgumentException, SQLException, DomainError {
        if(entity==null)
            throw new IllegalArgumentException("Null entity!");
        if(exists(entity.getId()))
            throw new RepoError("Entity already exists!");
        validator.validate(entity);
        entityInserter.add(entity);
    }

    /**
     * updates an entity in the repo
     *
     * @param entity the entity to update
     * @throws RepoError                if the entity doesn't exist
     * @throws IllegalArgumentException if the entity is null
     * @throws SQLException             if the database communication doesn't work
     * @throws DomainError              if the entity to be updated can't be validated
     */
    @Override
    public void update(E entity) throws RepoError, IllegalArgumentException, SQLException, DomainError {
        if(entity==null)
            throw new IllegalArgumentException("Null entity!");
        validator.validate(entity);
        if(!exists(entity.getId()))
            throw new RepoError("Entity doesn't exist!");
        entityUpdater.update(entity);
    }

    /**
     * removes an entity from the repository
     *
     * @param key the entity's key
     * @throws RepoError                if the entity doesn't exist
     * @throws IllegalArgumentException if key is null
     * @throws SQLException             if something went wrong in the DB communication
     */
    @Override
    public void remove(K key) throws RepoError, IllegalArgumentException, SQLException {
        if(key==null)
            throw new IllegalArgumentException("Null key!");
        if(!exists(key))
            throw new RepoError("Entity doesn't exist!");
        entityDeleter.delete(key);
    }
}
