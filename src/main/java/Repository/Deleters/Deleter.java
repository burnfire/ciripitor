package Repository.Deleters;

import java.sql.SQLException;

public interface Deleter<K> {
    /**
     * deletes an entity based on its' key
     * @param key entity's key
     */
    void delete(K key) throws SQLException;
}
