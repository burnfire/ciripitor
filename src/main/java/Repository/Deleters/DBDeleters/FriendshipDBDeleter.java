package Repository.Deleters.DBDeleters;

import Domain.Abstractization.Pair;

import java.sql.Connection;
import java.sql.SQLException;

public class FriendshipDBDeleter extends AbstractDBDeleter<Pair<Long>> {
    public FriendshipDBDeleter(Connection connection) {
        super(connection);
    }

    /**
     * deletes an entity based on its' key
     *
     * @param key entity's key
     */
    @Override
    public void delete(Pair<Long> key) throws SQLException {
        String query=String.format("DELETE FROM Friendships WHERE" +
                " sender=%d AND receiver=%d OR sender=%d AND receiver=%d",key.getLeft(),key.getRight(),key.getRight(),key.getLeft());
        connection.createStatement().execute(query);
    }
}
