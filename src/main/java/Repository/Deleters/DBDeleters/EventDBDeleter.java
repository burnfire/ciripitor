package Repository.Deleters.DBDeleters;

import java.sql.Connection;
import java.sql.SQLException;

public class EventDBDeleter extends AbstractDBDeleter<Long>{
    public EventDBDeleter(Connection connection) {
        super(connection);
    }

    /**
     * deletes an entity based on its' key
     *
     * @param key entity's key
     */
    @Override
    public void delete(Long key) throws SQLException {
        //We assume participations are cascade deleted
        String query = String.format("DELETE FROM events WHERE id=%d",key);
        connection.createStatement().execute(query);
    }
}
