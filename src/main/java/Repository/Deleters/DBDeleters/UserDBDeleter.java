package Repository.Deleters.DBDeleters;

import java.sql.Connection;
import java.sql.SQLException;

public class UserDBDeleter extends AbstractDBDeleter<Long>{
    public UserDBDeleter(Connection connection) {
        super(connection);
    }

    /**
     * deletes an entity based on its' key
     *
     * @param key entity's key
     */
    @Override
    public void delete(Long key) throws SQLException {
        //We assume UserSettings are deleted in cascade
        String query=String.format("DELETE FROM Users WHERE id=%d",key);
        connection.createStatement().execute(query);
    }
}
