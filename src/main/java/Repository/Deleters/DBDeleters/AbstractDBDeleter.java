package Repository.Deleters.DBDeleters;

import Repository.Deleters.Deleter;

import java.sql.Connection;

public abstract class AbstractDBDeleter<K> implements Deleter<K> {
    Connection connection;

    public AbstractDBDeleter(Connection connection) {
        this.connection = connection;
    }
}
