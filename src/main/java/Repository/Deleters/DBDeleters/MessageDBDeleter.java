package Repository.Deleters.DBDeleters;

import java.sql.Connection;
import java.sql.SQLException;

public class MessageDBDeleter extends AbstractDBDeleter<Long> {
    public MessageDBDeleter(Connection connection) {
        super(connection);
    }

    /**
     * deletes an entity based on its' key
     *
     * @param key entity's key
     */
    @Override
    public void delete(Long key) throws SQLException {
        String query=String.format("DELETE FROM messages WHERE id=%d",key);
        connection.createStatement().execute(query);
    }
}
