package Repository;

import Domain.Abstractization.Entity;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;

import java.sql.SQLException;
import java.util.List;

/**
 * models a homogenous repository with unique keys
 * @param <K> key type
 * @param <E> entity type
 */
public interface Repository<K,E extends Entity<K>> {

    /**
     * Checks if an entity exists
     * @param key - entity's key
     * @return true if the entity exists, false otherwise
     */
    boolean exists(K key) throws SQLException;

    /**
     * finds an element
     * @param key element's key
     * @return the element if it's found, null otherwise
     */
    E find(K key) throws SQLException;

    /**
     * gets all the elements in the repository
     * @return a {@link List} containing all the elements
     */
    List<E> getAll() throws SQLException;

    /**
     * adds an entity if it doesn't already exist
     * @param entity the entity to add
     * @throws RepoError if the entity already exists
     * @throws IllegalArgumentException if entity is null
     * @throws SQLException if the database communication doesn't work
     * @throws DomainError if the entity to be added can't be validated
     */
    void add(E entity) throws RepoError,IllegalArgumentException,SQLException,DomainError;

    /**
     * updates an entity in the repo
     * @param entity the entity to update
     * @throws RepoError if the entity doesn't exist
     * @throws IllegalArgumentException if the entity is null
     * @throws SQLException if the database communication doesn't work
     * @throws DomainError if the entity to be updated can't be validated
     */
    void update(E entity) throws RepoError,IllegalArgumentException,SQLException, DomainError;

    /**
     * removes an entity from the repository
     * @param key the entity's key
     * @throws RepoError if the entity doesn't exist
     * @throws IllegalArgumentException if key is null
     * @throws SQLException if something went wrong in the DB communication
     */
    void remove(K key) throws RepoError,IllegalArgumentException,SQLException;

    /**
     * counts the elements in the database
     * @return the number of entities in the database
     * @throws SQLException if something went wrong in the DB communication
     */
    Long size() throws SQLException;
}
