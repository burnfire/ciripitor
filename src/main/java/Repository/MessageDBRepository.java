package Repository;

import Domain.Message;
import Domain.User;
import Domain.Validators.Validator;
import Repository.Deleters.DBDeleters.MessageDBDeleter;
import Repository.Deleters.DBDeleters.UserDBDeleter;
import Repository.Inserters.DBInserters.MessageDBInserter;
import Repository.Inserters.DBInserters.UserDBInserter;
import Repository.Loaders.DBLoader.MessageDBLoader;
import Repository.Loaders.DBLoader.UserDBLoader;
import Repository.Searchers.DBSearcher.MessageDBSearcher;
import Repository.Searchers.DBSearcher.UserDBSearcher;
import Repository.Updaters.DBUpdaters.MessageDBUpdater;
import Repository.Updaters.DBUpdaters.UserDBUpdater;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MessageDBRepository extends AbstractDBRepository<Long, Message>{
    public MessageDBRepository(Validator<Message> validator, Connection connection) {
        super(validator,
                connection,
                new MessageDBSearcher(connection),
                new MessageDBLoader(connection),
                new MessageDBInserter(connection),
                new MessageDBUpdater(connection),
                new MessageDBDeleter(connection));
    }

    /**
     * counts the elements in the database
     *
     * @return the number of entities in the database
     * @throws SQLException if something went wrong in the DB communication
     */
    @Override
    public Long size() throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT Count(*) FROM friendships");
        resultSet.next();
        return resultSet.getLong(1);
    }
}
