package Repository;

import Domain.Abstractization.Pair;
import Domain.Friendships.Friendship;
import Domain.Validators.Validator;
import Repository.Deleters.DBDeleters.FriendshipDBDeleter;
import Repository.Inserters.DBInserters.FriendshipDBInserter;
import Repository.Loaders.DBLoader.FriendshipDBLoader;
import Repository.Loaders.Pager;
import Repository.Searchers.DBSearcher.FriendshipDBSearcher;
import Repository.Updaters.DBUpdaters.FriendshipDBUpdater;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class FriendshipDBRepository extends AbstractPageableDBRepository<Pair<Long>, Friendship> {
    public FriendshipDBRepository(Validator<Friendship> validator, Connection connection) {
        super(
                validator,
                connection,
                new FriendshipDBSearcher(connection),
                new FriendshipDBLoader(connection),
                new FriendshipDBInserter(connection),
                new FriendshipDBUpdater(connection),
                new FriendshipDBDeleter(connection));
    }

    /**
     * counts the elements in the database
     *
     * @return the number of entities in the database
     * @throws SQLException if something went wrong in the DB communication
     */
    @Override
    public Long size() throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT Count(*) FROM friendships");
        resultSet.next();
        return resultSet.getLong(1);
    }
}
