package Repository.Inserters;

import java.sql.SQLException;

public interface Inserter<E> {
    /**
     * adds an entity in the repository
     * @param entity the entity to be added
     */
    void add(E entity) throws SQLException;
}
