package Repository.Inserters.DBInserters;

import Domain.Events.Event;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class EventDBInserter extends AbstractDBInserter<Event>{
    public EventDBInserter(Connection connection) {
        super(connection);
    }

    /**
     * adds an entity in the repository
     *
     * @param entity the entity to be added
     */
    @Override
    public void add(Event entity) throws SQLException {
        insertMainEvent(entity);
        insertAttendees(entity);
    }

    private void insertAttendees(Event entity) throws SQLException {
        String query = "INSERT INTO participations(user_id,event_id) VALUES (%d,%d)";
        Statement statement = connection.createStatement();
        for(Long id:entity.getAttendees())
        {
            statement.executeQuery(String.format(query,entity.getId(),id));
        }
    }

    private void insertMainEvent(Event entity) throws SQLException {
        //We assume auto increment PK
        String mainQuery = String.format("INSERT INTO events(id,title,description,startdate,enddate,creator_id) VALUES (%d,'%s','%s','%s','%s',%d)",
                entity.getId(),
                entity.getTitle(),
                entity.getDescription(),
                entity.getStartDate().toString().replace('T',' '),
                entity.getEndDate().toString().replace('T',' '),
                entity.getCreatorId());
        connection.createStatement().execute(mainQuery);
    }
}
