package Repository.Inserters.DBInserters;

import Domain.Friendships.Friendship;

import java.sql.Connection;
import java.sql.SQLException;

public class FriendshipDBInserter extends AbstractDBInserter<Friendship> {
    public FriendshipDBInserter(Connection connection) {
        super(connection);
    }

    /**
     * adds an entity in the repository
     *
     * @param entity the entity to be added
     */
    @Override
    public void add(Friendship entity) throws SQLException {
        String query = String.format("INSERT INTO friendships(sender,receiver,datetime) VALUES (%d,%d,'%s');",
                entity.getId().getLeft(),
                entity.getId().getRight(),
                entity.getDateTime().toString().replace('T',' '));
        connection.createStatement().execute(query);
    }
}
