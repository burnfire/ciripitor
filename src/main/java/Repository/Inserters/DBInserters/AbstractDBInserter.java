package Repository.Inserters.DBInserters;

import Repository.Inserters.Inserter;

import java.sql.Connection;

public abstract class AbstractDBInserter<E> implements Inserter<E> {
    Connection connection;

    public AbstractDBInserter(Connection connection) {
        this.connection = connection;
    }
}
