package Repository.Inserters.DBInserters;

import Domain.Message;
import Domain.ReplyMessage;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class MessageDBInserter extends AbstractDBInserter<Message> {
    public MessageDBInserter(Connection connection) {
        super(connection);
    }

    /**
     * adds an entity in the repository
     *
     * @param entity the entity to be added
     */
    @Override
    public void add(Message entity) throws SQLException {
        insertMainMessage(entity);
        insertReceivers(entity);
    }

    private void insertReceivers(Message entity) throws SQLException {
        String query="INSERT INTO receivers(message_id,receiver_id) VALUES (%d,%d);";
        Statement statement = connection.createStatement();
        entity.getTo().forEach(u->
        {
            try {
                statement.execute(String.format(query,entity.getId(),u));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
    }

    private void insertMainMessage(Message entity) throws SQLException {
        String query = String.format("INSERT INTO messages(id,sender,message,datetime) VALUES (%d,%d,'%s','%s');",entity.getId(),entity.getFrom(),entity.getMessage(),entity.getDateTime().toString().replace('T',' '));
        connection.createStatement().execute(query);
        if(entity instanceof ReplyMessage)
        {
            query = String.format("UPDATE messages SET replyto=%d WHERE id=%d",((ReplyMessage) entity).getReplyTo(),entity.getId());
            connection.createStatement().execute(query);
        }
    }
}
