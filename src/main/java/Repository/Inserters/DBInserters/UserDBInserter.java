package Repository.Inserters.DBInserters;

import Domain.User;
import Utilities.Encryption;

import java.sql.Connection;
import java.sql.SQLException;

public class UserDBInserter extends AbstractDBInserter<User> implements Encryption {
    public UserDBInserter(Connection connection) {
        super(connection);
    }

    /**
     * adds an entity in the repository
     *
     * @param entity the entity to be added
     */
    @Override
    public void add(User entity) throws SQLException {
        addUser(entity);
        addUserSettings(entity.getId(),entity.getUserSettings());
    }

    /**
     * adds the user's settings to the DB
     * @param id user's id
     * @param userSettings user's settings
     * @throws SQLException if the DB connection went wrong
     */
    private void addUserSettings(Long id, User.UserSettings userSettings) throws SQLException {
        String query = String.format("INSERT INTO user_settings(id,eventsnotifications) VALUES (%d,%b);",
                id,
                userSettings.isReceiveEventsNotifications()
        );
        connection.createStatement().execute(query);
    }

    /**
     * adds only the user into the DB
     * @param entity the user to add
     * @throws SQLException if the DB connection went wrong
     */
    private void addUser(User entity) throws SQLException {
        String query = String.format("INSERT INTO users(id,firstname,lastname,nickname,password,email) VALUES (%d,'%s','%s','%s','%s','%s');",
                entity.getId(),
                encryptString(entity.getFirstName()),
                encryptString(entity.getLastName()),
                encryptString(entity.getNickname()),
                encryptString(entity.getPasswd()),
                encryptString(entity.getEmail()));
        connection.createStatement().execute(query);
    }
}
