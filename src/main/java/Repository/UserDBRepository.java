package Repository;

import Domain.User;
import Domain.Validators.Validator;
import Repository.Deleters.DBDeleters.AbstractDBDeleter;
import Repository.Deleters.DBDeleters.UserDBDeleter;
import Repository.Inserters.DBInserters.AbstractDBInserter;
import Repository.Inserters.DBInserters.UserDBInserter;
import Repository.Loaders.DBLoader.AbstractDBLoader;
import Repository.Loaders.DBLoader.UserDBLoader;
import Repository.Searchers.DBSearcher.AbstractDBSearcher;
import Repository.Searchers.DBSearcher.UserDBSearcher;
import Repository.Updaters.DBUpdaters.AbstractDBUpdater;
import Repository.Updaters.DBUpdaters.UserDBUpdater;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDBRepository extends AbstractDBRepository<Long, User>{
    public UserDBRepository(Validator<User> validator, Connection connection) {
        super(validator,
                connection,
                new UserDBSearcher(connection),
                new UserDBLoader(connection),
                new UserDBInserter(connection),
                new UserDBUpdater(connection),
                new UserDBDeleter(connection));
    }

    /**
     * counts the elements in the database
     *
     * @return the number of entities in the database
     * @throws SQLException if something went wrong in the DB communication
     */
    @Override
    public Long size() throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT Count(*) FROM users");
        resultSet.next();
        return resultSet.getLong(1);
    }
}
