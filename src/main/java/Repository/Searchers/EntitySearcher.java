package Repository.Searchers;

import java.sql.SQLException;

public interface EntitySearcher<K,E> {
    /**
     * searches an entity by its' key
     * @param key entity's key
     * @return the element if it exists, null otherwise
     * @throws IllegalArgumentException if key is null
     * @throws SQLException if DB communication fails
     */
    E search(K key) throws IllegalArgumentException, SQLException;
}
