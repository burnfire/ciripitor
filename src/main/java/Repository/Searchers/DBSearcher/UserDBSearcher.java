package Repository.Searchers.DBSearcher;

import Domain.User;
import Utilities.Encryption;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDBSearcher extends AbstractDBSearcher<Long,User> implements Encryption {
    public UserDBSearcher(Connection connection) {
        super(connection);
    }

    /**
     * searches an entity by its' key
     *
     * @param key entity's key
     * @return the element if it exists, null otherwise
     * @throws IllegalArgumentException if key is null
     * @throws SQLException             if DB communication fails
     */
    @Override
    public User search(Long key) throws IllegalArgumentException, SQLException {
        User user;
        String query = String.format("SELECT users.id,firstname,lastname,nickname,password,email,eventsnotifications FROM users INNER JOIN user_settings ON users.id=user_settings.id WHERE users.id=%d",key);
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        if(!resultSet.next())
            return null;
        user=new User(
                resultSet.getLong(1),
                decryptString(resultSet.getString(2)),
                decryptString(resultSet.getString(3)),
                decryptString(resultSet.getString(4)),
                decryptString(resultSet.getString(5)),
                decryptString(resultSet.getString(6)));
        User.UserSettings userSettings = new User.UserSettings(resultSet.getBoolean(7));
        user.setUserSettings(userSettings);
        return user;
    }
}
