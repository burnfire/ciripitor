package Repository.Searchers.DBSearcher;

import Repository.Searchers.EntitySearcher;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class AbstractDBSearcher<K,E> implements EntitySearcher<K, E> {
    protected Connection connection;
    public AbstractDBSearcher(Connection connection) {
        this.connection = connection;
    }


}
