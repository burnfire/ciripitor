package Repository.Searchers.DBSearcher;

import Domain.Abstractization.Pair;
import Domain.Friendships.Friendship;
import Domain.Friendships.FriendshipStatus;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FriendshipDBSearcher extends AbstractDBSearcher<Pair<Long>, Friendship> {
    public FriendshipDBSearcher(Connection connection) {
        super(connection);
    }

    /**
     * searches an entity by its' key
     *
     * @param key entity's key
     * @return the element if it exists, null otherwise
     * @throws IllegalArgumentException if key is null
     * @throws SQLException             if DB communication fails
     */
    @Override
    public Friendship search(Pair<Long> key) throws IllegalArgumentException, SQLException {
        Friendship friendship;
        String query = String.format("SELECT sender,receiver,datetime,status FROM %s WHERE sender=%d AND receiver=%d OR sender=%d AND receiver=%d","friendships",key.getLeft(),key.getRight(),key.getRight(),key.getLeft());
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        if(!resultSet.next())
            return null;
        friendship=new Friendship(
                resultSet.getLong(1),
                resultSet.getLong(2),
                resultSet.getTimestamp(3).toLocalDateTime(),
                (resultSet.getInt(4)==0? FriendshipStatus.PENDING: FriendshipStatus.ACCEPTED)
                );
        return friendship;
    }
}
