package Repository.Searchers.DBSearcher;

import Domain.Events.Event;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventDBSearcher extends AbstractDBSearcher<Long, Event>{
    public EventDBSearcher(Connection connection) {
        super(connection);
    }

    /**
     * searches an entity by its' key
     *
     * @param key entity's key
     * @return the element if it exists, null otherwise
     * @throws IllegalArgumentException if key is null
     * @throws SQLException             if DB communication fails
     */
    @Override
    public Event search(Long key) throws IllegalArgumentException, SQLException {
        if(key==null)
            throw new IllegalArgumentException("Null key!");
        String query = String.format("SELECT id,title,description,startdate,enddate,creator_id FROM Events WHERE id=%d", key);
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        if(!resultSet.next())
            return null;
        Event event = new Event(
                resultSet.getLong(1),
                resultSet.getString(2),
                resultSet.getString(3),
                resultSet.getTimestamp(4).toLocalDateTime(),
                resultSet.getTimestamp(5).toLocalDateTime(),
                resultSet.getLong(6),
                getAttendees(key));
        return event;
    }

    private List<Long> getAttendees(Long key) throws SQLException {
        List<Long> userIds = new ArrayList<>();
        String query = String.format("SELECT user_id FROM participations WHERE event_id=%d",key);
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while (resultSet.next())
        {
            userIds.add(resultSet.getLong(1));
        }
        return userIds;
    }
}
