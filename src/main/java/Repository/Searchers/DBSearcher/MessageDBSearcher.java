package Repository.Searchers.DBSearcher;

import Domain.Message;
import Domain.ReplyMessage;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageDBSearcher extends AbstractDBSearcher<Long, Message> {
    public MessageDBSearcher(Connection connection) {
        super(connection);
    }

    /**
     * searches an entity by its' key
     *
     * @param key entity's key
     * @return the element if it exists, null otherwise
     * @throws IllegalArgumentException if key is null
     * @throws SQLException             if DB communication fails
     */
    @Override
    public Message search(Long key) throws IllegalArgumentException, SQLException {
        Message message;
        String query = String.format("SELECT id,sender,message,datetime,replyto FROM messages WHERE id=%d",key);
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        if(!resultSet.next())
            return null;
        if(resultSet.getLong(5)==0)
            message=new Message(
                resultSet.getString(3),
                resultSet.getLong(2),
                getReceivers(key),
                resultSet.getTimestamp(4).toLocalDateTime()
                );
        else
            message=new ReplyMessage(
                    resultSet.getString(3),
                    resultSet.getLong(2),
                    getReceivers(key),
                    resultSet.getTimestamp(4).toLocalDateTime(),
                    resultSet.getLong(5)
            );
        message.setId(key);
        return message;
    }

    private List<Long> getReceivers(Long key) throws SQLException {
        List<Long> all = new ArrayList<>();
        String query = String.format("SELECT receiver_id FROM receivers WHERE message_id=%d",key);
        ResultSet resultSet = connection.createStatement().executeQuery(query);
        while(resultSet.next())
        {
            all.add(resultSet.getLong(1));
        }
        return all;
    }
}
