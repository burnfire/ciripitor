package Repository.Updaters;

import java.sql.SQLException;

public interface Updater<E> {

    /**
     * updates an entity in the repository
     * @param entity entity to be updated
     */
    void update(E entity) throws SQLException;
}
