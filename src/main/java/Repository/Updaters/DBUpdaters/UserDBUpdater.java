package Repository.Updaters.DBUpdaters;

import Domain.User;
import Utilities.Encryption;

import java.sql.Connection;
import java.sql.SQLException;

public class UserDBUpdater extends AbstractDBUpdater<User> implements Encryption {
    public UserDBUpdater(Connection connection) {
        super(connection);
    }

    /**
     * updates an entity in the repository
     *
     * @param entity entity to be updated
     */
    @Override
    public void update(User entity) throws SQLException {
        updateUser(entity);
        updateUserSettings(entity.getId(),entity.getUserSettings());
    }

    /**
     * updates only the user's settings
     * @param id user's id
     * @param userSettings user's settings
     * @throws SQLException if the DB connection went wrong
     */
    private void updateUserSettings(Long id, User.UserSettings userSettings) throws SQLException {
        String query = String.format("UPDATE user_settings SET eventsnotifications=%b WHERE id=%d",
                userSettings.isReceiveEventsNotifications(),
                id
        );
        connection.createStatement().execute(query);
    }

    /**
     * updates only the user in the DB
     * @param entity user to update
     * @throws SQLException if the DB connection went wrong
     */
    private void updateUser(User entity) throws SQLException {
        String query = String.format("UPDATE users SET firstname='%s', lastname='%s', nickname='%s', password='%s', email='%s' WHERE id=%d",
                encryptString(entity.getFirstName()),
                encryptString(entity.getLastName()),
                encryptString(entity.getNickname()),
                encryptString(entity.getPasswd()),
                encryptString(entity.getEmail()),
                entity.getId());
        connection.createStatement().execute(query);
    }
}
