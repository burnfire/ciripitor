package Repository.Updaters.DBUpdaters;

import Domain.Friendships.Friendship;

import java.sql.Connection;
import java.sql.SQLException;

public class FriendshipDBUpdater extends AbstractDBUpdater<Friendship> {
    public FriendshipDBUpdater(Connection connection) {
        super(connection);
    }

    /**
     * updates an entity in the repository
     *
     * @param entity entity to be updated
     */
    @Override
    public void update(Friendship entity) throws SQLException {
        String query = String.format("UPDATE friendships SET sender=%d,receiver=%d,datetime='%s',status=%d WHERE sender=%d AND receiver=%d OR sender=%d AND receiver=%d",
                entity.getId().getLeft(),
                entity.getId().getRight(),
                entity.getDateTime().toString().replace('T',' '),
                (entity.isAccepted()?1:0),
                entity.getId().getLeft(),
                entity.getId().getRight(),
                entity.getId().getRight(),
                entity.getId().getLeft()
                );
        connection.createStatement().execute(query);
    }
}
