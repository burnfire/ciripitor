package Repository.Updaters.DBUpdaters;

import Domain.Message;
import Domain.ReplyMessage;
import Domain.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class MessageDBUpdater extends AbstractDBUpdater<Message> {
    public MessageDBUpdater(Connection connection) {
        super(connection);
    }

    /**
     * updates an entity in the repository
     *
     * @param entity entity to be updated
     */
    @Override
    public void update(Message entity) throws SQLException {
        String query = String.format("UPDATE messages SET id=%d, sender=%d, message='%s', datetime='%s' WHERE id=%d",
                entity.getId(),
                entity.getFrom(),
                entity.getMessage(),
                entity.getDateTime().toString().replace('T',' '),
                entity.getId());
        connection.createStatement().execute(query);
        if(entity instanceof ReplyMessage)
        {
            query=String.format("UPDATE messages SET replyto=%d WHERE id=%d",((ReplyMessage) entity).getReplyTo(),entity.getId());
            connection.createStatement().execute(query);
        }
        String deleteReceivers = String.format("DELETE FROM receivers WHERE message_id=%d",entity.getId());
        connection.createStatement().execute(deleteReceivers);
        String insertReceivers = "INSERT INTO receivers(message_id,receiver_id) VALUES(%d,%d);";
        Statement statement = connection.createStatement();
        entity.getTo().forEach(u->
        {
            try {
                statement.execute(String.format(insertReceivers,entity.getId(),u));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });
    }
}
