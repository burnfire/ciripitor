package Repository.Updaters.DBUpdaters;

import Domain.Events.Event;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class EventDBUpdater extends AbstractDBUpdater<Event>{
    public EventDBUpdater(Connection connection) {
        super(connection);
    }

    /**
     * updates an entity in the repository
     *
     * @param entity entity to be updated
     */
    @Override
    public void update(Event entity) throws SQLException {
        deleteParticipations(entity);
        updateEvent(entity);
        addParticipations(entity);
    }

    private void addParticipations(Event entity) throws SQLException {
        String query = "INSERT INTO participations(user_id,event_id) VALUES (%d,%d)";
        Statement statement = connection.createStatement();
        for(Long id:entity.getAttendees())
        {
            statement.execute(String.format(query,id,entity.getId()));
        }
    }

    private void updateEvent(Event entity) throws SQLException {
        String query = String.format("UPDATE events SET title='%s', description='%s', startdate='%s', enddate='%s', creator_id=%d WHERE id=%d",
                entity.getTitle(),
                entity.getDescription(),
                entity.getStartDate().toString(),
                entity.getEndDate().toString(),
                entity.getCreatorId(),
                entity.getId());
        connection.createStatement().execute(query);
    }

    private void deleteParticipations(Event entity) throws SQLException {
        String query = String.format("DELETE FROM participations WHERE event_id=%d",entity.getId());
        connection.createStatement().execute(query);
    }
}
