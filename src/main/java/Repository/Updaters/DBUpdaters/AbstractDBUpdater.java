package Repository.Updaters.DBUpdaters;

import Repository.Updaters.Updater;

import java.sql.Connection;

public abstract class AbstractDBUpdater<E> implements Updater<E> {
    Connection connection;

    public AbstractDBUpdater(Connection connection) {
        this.connection = connection;
    }
}
