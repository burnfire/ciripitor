package Repository;

import Domain.Abstractization.Entity;

import java.sql.SQLException;
import java.util.List;

public interface PageableRepository<K,E extends Entity<K>> extends Repository<K, E> {
    /**
     * gets all entities from a page
     * @param pageSize page's size
     * @param pageNo page's number
     * @return a {@link List<E>} containing the desired entities
     */
    public List<E> getAll(int pageSize,int pageNo) throws SQLException;
}
