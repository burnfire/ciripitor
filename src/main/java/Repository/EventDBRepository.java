package Repository;

import Domain.Events.Event;
import Domain.Validators.Validator;
import Repository.Deleters.DBDeleters.AbstractDBDeleter;
import Repository.Deleters.DBDeleters.EventDBDeleter;
import Repository.Inserters.DBInserters.AbstractDBInserter;
import Repository.Inserters.DBInserters.EventDBInserter;
import Repository.Loaders.DBLoader.AbstractDBLoader;
import Repository.Loaders.DBLoader.EventDBLoader;
import Repository.Searchers.DBSearcher.AbstractDBSearcher;
import Repository.Searchers.DBSearcher.EventDBSearcher;
import Repository.Updaters.DBUpdaters.AbstractDBUpdater;
import Repository.Updaters.DBUpdaters.EventDBUpdater;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EventDBRepository extends AbstractDBRepository<Long, Event>{
    public EventDBRepository(Validator<Event> validator, Connection connection) {
        super(validator, connection, new EventDBSearcher(connection), new EventDBLoader(connection), new EventDBInserter(connection), new EventDBUpdater(connection), new EventDBDeleter(connection));
    }

    /**
     * counts the elements in the database
     *
     * @return the number of entities in the database
     * @throws SQLException if something went wrong in the DB communication
     */
    @Override
    public Long size() throws SQLException {
        ResultSet resultSet = connection.createStatement().executeQuery("SELECT Count(*) FROM events");
        resultSet.next();
        return resultSet.getLong(1);
    }
}
