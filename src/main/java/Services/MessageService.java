package Services;

import Domain.Abstractization.Entity;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Message;
import Domain.Observer.Observable;
import Domain.ReplyMessage;
import Domain.User;
import Repository.Repository;
import Services.DTOs.MessageDTO;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class MessageService extends Observable {
    Repository<Long, User> userRepo;
    Repository<Long, Message> messageRepo;
    Long id;

    public MessageService(Repository<Long, User> userRepo, Repository<Long, Message> messageRepo) throws SQLException {
        this.userRepo = userRepo;
        this.messageRepo = messageRepo;
        setAvailableId();
    }

    protected Long getAvailableId() {
        return id++;
    }

    /**
     * sends a message from an user to one or more users
     * @param senderId sender's id
     * @param receiverIds receiver's id
     * @param message message's text
     * @throws SQLException if the DB connection went wrong
     * @throws ServiceError if either sender or one of receivers don't exist
     * @throws DomainError if the message isn't valid
     * @throws RepoError if the message already exists
     */
    public void sendMessage(Long senderId, List<Long> receiverIds, String message) throws SQLException, ServiceError, DomainError, RepoError {
        if(!userRepo.exists(senderId))
            throw new ServiceError("Sender doesn't exist!");
        if(receiverIds.stream().noneMatch(u->{
            try {
                return userRepo.exists(u);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            return false;
        }))
            throw new ServiceError("Receiver doesn't exist!");
        messageRepo.add(new Message(getAvailableId(),message,senderId,receiverIds, LocalDateTime.now()));
        notifyObservers();
    }

    /**
     * sends a message to a single user
     * @param senderId sender's id
     * @param receiverId receiver's id
     * @param message message's text
     * @throws ServiceError
     * @throws SQLException
     * @throws DomainError
     * @throws RepoError
     */
    public void sendMessage(Long senderId, Long receiverId, String message) throws ServiceError, SQLException, DomainError, RepoError {
        List<Long> dummy = new ArrayList<>();
        dummy.add(receiverId);
        sendMessage(senderId,dummy,message);
        notifyObservers();
    }

    /**
     * replies to a message, if it exists
     * @param senderId reply message's sender
     * @param messageId id of the message to reply
     * @param message reply message's
     * @throws SQLException if the DB connection went wrong
     * @throws ServiceError if either sender of the reply or the message to be replied don't exist
     * @throws DomainError if the reply message isn't valid
     * @throws RepoError if the reply message already exists
     */
    public void replyToMessage(Long senderId,Long messageId,String message) throws SQLException, ServiceError, DomainError, RepoError {
        if(!userRepo.exists(senderId))
            throw new ServiceError("Sender doesn't exist!");
        Message potentialMessage = messageRepo.find(messageId);
        if(potentialMessage==null)
            throw new ServiceError("Message doesn't exist!");
        if(potentialMessage.getTo().stream().noneMatch(u->{return u.equals(senderId);}) && !potentialMessage.getFrom().equals(senderId))
            throw new ServiceError("Message wasn't meant for you to reply!");
        messageRepo.add(new ReplyMessage(potentialMessage.getId(), getAvailableId(), message, senderId, potentialMessage.getFrom(), LocalDateTime.now()));
        notifyObservers();
    }

    /**
     * gets the messages between users having mentioned ids
     * @param id1 first user's id
     * @param id2 second user's id
     * @return
     * @throws SQLException
     */
    public List<MessageDTO> getMessagesBetweenUsers(Long id1, Long id2) throws SQLException {
        List<Message> all =  messageRepo.getAll().stream()
                .filter(m->
        {
            return (m.getFrom().equals(id1) && m.getTo().contains(id2)) || (m.getFrom().equals(id2) && m.getTo().contains(id1));
        })
                .collect(Collectors.toList());
        List<MessageDTO> rez = new ArrayList<>();
        for(Message m:all)
        {
            rez.add(messageToDTO(m));
        }
        return rez;
    }

    protected void setAvailableId() throws SQLException {
        Optional<Long> dbid = messageRepo.getAll().stream().map(Entity::getId).max(Long::compareTo);
        id = dbid.orElse(1L);
        id++;
    }

    /**
     * gets all users a user has chatted with
     * @param userId user's id that I want to see who he has chatted with
     * @return a {@link Set<User>} containing all users an user has chatted with. Being a Set, users are guaranteed to be unique
     * @throws SQLException if the DB connection went wrong
     */
    public Set<User> getUsersThatChattedWithUser(Long userId) throws SQLException {
        Set<User> rez = new HashSet<>();
        messageRepo.getAll().stream()
                .filter(m->{return m.getFrom().equals(userId) || m.getTo().contains(userId);})
                .forEach(m->
                {
                    m.getTo().forEach(u-> {
                        try {
                            if(!u.equals(userId))
                                rez.add(userRepo.find(u));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    });
                    if(!m.getFrom().equals(userId)) {
                        try {
                            rez.add(userRepo.find(m.getFrom()));
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                });
        return rez;
    }

    /**
     * gets all messages that a user has sent
     * @param userId sender's id
     * @return {@link List<MessageDTO>} that contains the messages an user sent
     * @throws SQLException
     */
    public List<MessageDTO> getMessagesFromUser(Long userId) throws SQLException {
        List<MessageDTO> all = new ArrayList<>();
        for(Message m:messageRepo.getAll().stream()
                .filter(m->
                {
                    return m.getFrom().equals(userId) || m.getTo().contains(userId);
                }).collect(Collectors.toList()))
        {
            MessageDTO msg = messageToDTO(m);
            all.add(msg);
        }
        return all;
    }

    /**
     * takes a message and transforms it in a DTO
     * @param m the bare {@link Message}
     * @return {@link MessageDTO}
     * @throws SQLException
     */
    private MessageDTO messageToDTO(Message m) throws SQLException {
        MessageDTO msg = new MessageDTO(
                m.getId(),
                userRepo.find(m.getFrom()),
                m.getTo().stream()
                        .map(u->
                        {
                            try {
                                return userRepo.find(u);
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
                            return new User(-1L,"","","","","");
                        }).collect(Collectors.toList()),
                m.getMessage(),
                m.getDateTime(),
                null
        );
        if(m instanceof ReplyMessage)
            msg.setReplyTo(messageRepo.find(((ReplyMessage) m).getReplyTo()));
        return msg;
    }
}
