package Services;

import Domain.Abstractization.Entity;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Events.Event;
import Domain.Observer.Observable;
import Domain.User;
import Repository.Repository;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EventService extends Observable{
    Repository<Long, Event> eventRepo;
    Repository<Long, User> userRepo;
    Long id;

    public EventService(Repository<Long, Event> eventRepo, Repository<Long, User> userRepo) throws SQLException {
        this.eventRepo = eventRepo;
        this.userRepo = userRepo;
        setAvailableId();
    }

    private void setAvailableId() throws SQLException {
        Optional<Long> dbid = eventRepo.getAll().stream().map(Entity::getId).max(Long::compareTo);
        id = dbid.orElse(1L);
        id++;
    }

    /**
     * creates an event
     * @param creatorId id of the user that created the event
     * @param title event's title
     * @param description event's description
     * @param startDate event's start date
     * @param endDate event's end date
     * @throws SQLException
     * @throws ServiceError if the user having id {@param creatorId} doesn't exist
     * @throws DomainError if the created event isn't valid
     * @throws RepoError
     */
    public void createEvent(Long creatorId,
                            String title,
                            String description,
                            LocalDateTime startDate,
                            LocalDateTime endDate) throws SQLException, ServiceError, DomainError, RepoError {
        if(!userRepo.exists(creatorId))
            throw new ServiceError("User doesn't exist!");
        Event event = new Event(getAvailableId(),title,description,startDate,endDate,creatorId,new ArrayList<>());
        eventRepo.add(event);
        notifyObservers();
    }

    /**
     * gets an available ID for a new event to have
     * @return a {@link Long} having a valid id for an event
     */
    private Long getAvailableId() {
        return id++;
    }

    /**
     * adds an user's id to the attendees list of a specified event
     * @param eventId event's id
     * @param attendeeId user to attend id
     * @throws SQLException
     * @throws ServiceError if either event or user don't exist or if user already attends this event
     * @throws DomainError
     * @throws RepoError
     */
    public void attendEvent(Long eventId,Long attendeeId) throws SQLException, ServiceError, DomainError, RepoError {
        if(!eventRepo.exists(eventId))
            throw new ServiceError("Event doesn't exist!");
        if(!userRepo.exists(attendeeId))
            throw new ServiceError("User doesn't exist!");
        Event event = eventRepo.find(eventId);
        if(event.getAttendees().contains(attendeeId))
            throw new ServiceError("User already attends this event!");
        event.addAttendee(attendeeId);
        eventRepo.update(event);
        notifyObservers();
    }

    /**
     * removes an attendee from an event's attendees list
     * @param eventId event's id
     * @param attendeeId user to remove from attendee's list
     * @throws SQLException
     * @throws ServiceError if either event or user don't exist, or if user doesn't attend this event
     * @throws DomainError
     * @throws RepoError
     */
    public void removeAttendance(Long eventId, Long attendeeId) throws SQLException, ServiceError, DomainError, RepoError {
        if(!eventRepo.exists(eventId))
            throw new ServiceError("Event doesn't exist!");
        if(!userRepo.exists(attendeeId))
            throw new ServiceError("User doesn't exist!");
        Event event = eventRepo.find(eventId);
        if(!event.getAttendees().contains(attendeeId))
            throw new ServiceError("User doesn't attend this event!");
        event.removeAttendee(attendeeId);
        eventRepo.update(event);
        notifyObservers();
    }

    /**
     * removes an event
     * @param eventId event's id
     * @throws SQLException
     * @throws ServiceError if the event doesn't exist
     * @throws RepoError
     */
    public void removeEvent(Long eventId) throws SQLException, ServiceError, RepoError {
        if(!eventRepo.exists(eventId))
            throw new ServiceError("Event doesn't exist!");
        eventRepo.remove(eventId);
        notifyObservers();
    }

    /**
     * gets the events that an user attends to
     * @param userId user's id
     * @return a {@link List<Event>} that contains all events an user attends
     * @throws SQLException
     * @throws ServiceError if the user doesn't exist
     */
    public List<Event> getUserEvents(Long userId) throws SQLException, ServiceError {
        if(!userRepo.exists(userId))
            throw new ServiceError("User doesn't exist!");
        return eventRepo.getAll()
                .stream()
                .filter(e->
                {
                    return e.getAttendees().contains(userId);
                })
                .collect(Collectors.toList());
    }

    /**
     * gets all events an user created
     * @param userId user's id
     * @return a {@link List<Event>} containing all events an user has created
     * @throws SQLException
     * @throws ServiceError
     */
    public List<Event> getEventsUserCreated(Long userId) throws SQLException, ServiceError {
        if(!userRepo.exists(userId))
            throw new ServiceError("User doesn't exist!");
        return eventRepo.getAll()
                .stream()
                .filter(e->
                {
                    return e.getCreatorId().equals(userId);
                })
                .collect(Collectors.toList());
    }

    /**
     * gets all events
     * @return a {@link List<Event>} containing all events
     * @throws SQLException
     */
    public List<Event> getAllEvents() throws SQLException {
        return eventRepo.getAll();
    }

    /**
     * gets an event by its id
     * @param id event's id
     * @return an {@link Event} having the desired id
     * @throws SQLException
     * @throws ServiceError if the event having the event id {@param id} doesn't exist
     */
    public Event getEvent(Long id) throws SQLException, ServiceError {
        Optional<Event> event = getAllEvents().stream().filter(e->{return e.getId().equals(id);}).findFirst();
        if(!event.isPresent())
            throw new ServiceError("Event having this id doesn't exist!");
        return event.get();
    }
}
