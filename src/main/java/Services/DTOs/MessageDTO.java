package Services.DTOs;

import Domain.Message;
import Domain.User;

import java.time.LocalDateTime;
import java.util.List;

public class MessageDTO {
    Long id;
    User sender;
    List<User> receivers;
    String message;
    LocalDateTime datetime;
    Message replyTo;

    public MessageDTO(Long id, User sender, List<User> receivers, String message, LocalDateTime datetime, Message replyTo) {
        this.id=id;
        this.sender = sender;
        this.receivers = receivers;
        this.message = message;
        this.datetime = datetime;
        this.replyTo = replyTo;
    }

    public void setReplyTo(Message replyTo) {
        this.replyTo = replyTo;
    }

    public User getSender() {
        return sender;
    }

    public List<User> getReceivers() {
        return receivers;
    }

    public String getMessage() {
        return message;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public Message getReplyTo() {
        return replyTo;
    }

    public Long getId() {
        return id;
    }
}
