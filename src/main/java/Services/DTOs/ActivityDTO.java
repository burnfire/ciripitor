package Services.DTOs;

import Domain.Profiles.PrivateProfile;
import Domain.Profiles.PublicProfile;

import java.time.LocalDateTime;

public class ActivityDTO {
    String description;
    LocalDateTime dateTime;

    public ActivityDTO(String description, LocalDateTime dateTime) {
        this.description = description;
        this.dateTime = dateTime;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
