package Services.DTOs;

import Domain.Friendships.FriendshipStatus;
import Domain.User;

import java.time.LocalDateTime;

public class FriendshipDTO {
    User sender,receiver;
    LocalDateTime datetime;
    FriendshipStatus status;

    public FriendshipDTO(User sender, User receiver, LocalDateTime datetime, FriendshipStatus status) {
        this.sender = sender;
        this.receiver = receiver;
        this.datetime = datetime;
        this.status = status;
    }

    public User getSender() {
        return sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public FriendshipStatus getStatus() {
        return status;
    }
}
