package Services;

import Domain.Abstractization.Entity;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Observer.Observable;
import Domain.User;
import Repository.Repository;
import Utilities.Encryption;
import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class UserService extends Observable{
    Repository<Long, User> userRepo;
    Long id;

    //Defaults
    final boolean sendEventsNotifications=true;

    public UserService(Repository<Long, User> userRepo) throws SQLException { ;
        this.userRepo = userRepo;
        setAvailableId();
    }

    protected Long getAvailableId() {
        return id++;
    }

    /**
     * computes the first available id
     * @throws SQLException
     */
    protected void setAvailableId() throws SQLException {
        Optional<Long> dbid = userRepo.getAll().stream().map(Entity::getId).max(Long::compareTo);
        id = dbid.orElse(1L);
        id++;
    }

    /**
     * registers a user in the app
     * @param firstName
     * @param lastName
     * @param nickname
     * @param password
     * @throws RepoError
     * @throws SQLException
     * @throws DomainError
     */
    public void register(String firstName,String lastName,String nickname,String password,String email) throws RepoError, SQLException, DomainError, ServiceError {
        if(userRepo.getAll().stream().anyMatch(u->{return u.getNickname().equals(nickname) || u.getEmail().equals(email);}))
            throw new ServiceError("User having this nickname or email address already exist!");
        User user = new User(getAvailableId(),firstName,lastName,nickname,password,email);
        user.setUserSettings(new User.UserSettings(sendEventsNotifications));
        userRepo.add(user);
        notifyObservers();
    }

    /**
     * removes a user by his key
     * @param key user's key
     * @throws SQLException
     * @throws RepoError
     */
    public void removeUser(Long key) throws SQLException, RepoError, ServiceError {
        userRepo.remove(key);
        notifyObservers();
    }

    /**
     * removes user by his nickname
     * @param nickname user's nickname
     * @throws SQLException
     * @throws RepoError
     * @throws ServiceError
     */
    public void removeUser(String nickname) throws SQLException, RepoError, ServiceError {
        Optional<User> id = getUserByNickname(nickname);
        if(!id.isPresent())
            throw new ServiceError("User having this nickname doesn't exist!");
        userRepo.remove(id.get().getId());
        notifyObservers();
    }

    /**
     * updates an user's names
     * if either firstName or lastName is null or empty, they aren't changed
     * @param key user's id
     * @param firstName user's new first name
     * @param lastName user's new last name
     * @throws SQLException
     * @throws ServiceError
     * @throws DomainError
     * @throws RepoError
     */
    public void setUserName(Long key,String firstName,String lastName) throws SQLException, ServiceError, DomainError, RepoError {
        User user = userRepo.find(key);
        if(user==null)
            throw new ServiceError("User doesn't exist!");
        if(firstName!=null && !firstName.isEmpty())
            user.setFirstName(firstName);
        if(lastName!=null && !lastName.isEmpty())
            user.setLastName(lastName);
        userRepo.update(user);
        notifyObservers();
    }

    /**
     * updates an user's nickname
     * @param key user's id
     * @param nickname user's new nickname
     * @throws SQLException
     * @throws ServiceError
     * @throws DomainError
     * @throws RepoError
     */
    public void setNickname(Long key,String nickname) throws SQLException, ServiceError, DomainError, RepoError {
        User user = userRepo.find(key);
        if(user==null)
            throw new ServiceError("User doesn't exist!");
        user.setNickname(nickname);
        userRepo.update(user);
        notifyObservers();
    }

    /**
     * changes a user's password
     * @param key user's id
     * @param password user's new password
     * @throws SQLException
     * @throws DomainError
     * @throws RepoError
     * @throws ServiceError
     */
    public void setPassword(Long key,String password) throws SQLException, DomainError, RepoError, ServiceError {
        User user = userRepo.find(key);
        if(user==null)
            throw new ServiceError("User doesn't exist!");
        user.setPasswd(password);
        userRepo.update(user);
        notifyObservers();
    }

    /**
     * finds an user having a nickname and a password
     * @param nickname user's nickname or email address
     * @param password user's password
     * @return the User if the authentication is successful
     * @throws SQLException if the DB connection isn't successful
     * @throws ServiceError if authentication fails
     */
    public User login(String nickname, String password) throws SQLException, ServiceError {
        Optional<User> user = userRepo.getAll().stream().filter(u->
        {
            return u.getNickname().equals(nickname) || u.getEmail().equals(nickname);
        }).findFirst();
        if(!user.isPresent())
            throw new ServiceError("User having this nickname or email address doesn't exist!");
        if(!user.get().getPasswd().equals(password))
            throw new ServiceError("Incorrect password!");
        return user.get();
    }

    /**
     * finds an user by his nickname
     * @param nickname user's (partial) nickname
     * @return an {@link Optional<User>} containing the desired user
     * @throws SQLException
     * @throws IllegalArgumentException if nickname is null
     */
    public Optional<User> getUserByNickname(String nickname) throws SQLException {
        if(nickname==null)
            throw new IllegalArgumentException("Null nickname!");
        return userRepo.getAll().stream()
                .filter(u ->
                {
                    return u.getNickname().toLowerCase(Locale.ROOT).contains(nickname.toLowerCase(Locale.ROOT));
                    //return u.getNickname().equals(nickname);
                })
                .findFirst();
    }

    /**
     * filters users by a given condition
     * @param condition condition to accept users
     * @return a {@link List<User>} having only users that match {@param condition}
     * @throws SQLException
     */
    List<User> filterUsers(Predicate<User> condition) throws SQLException {
        return userRepo.getAll().stream().filter(condition).collect(Collectors.toList());
    }

    /**
     * gets all users having a partial match on nickname
     * @param nickname user's (partial) nickname
     * @return a {@link List<User>} having all users with a partial match with the given nickname
     * @throws SQLException
     */
    public List<User> filterUsersNickname(String nickname) throws SQLException {
        Predicate<User> predicate = user -> user.getNickname().toLowerCase(Locale.ROOT).contains(nickname.toLowerCase(Locale.ROOT));
        return filterUsers(predicate);
    }

    /**
     * gets an user by its id
     * @param id user's id
     * @return the user having the given id
     * @throws SQLException
     * @throws ServiceError if the user having this id doesn't exist
     */
    public User getUserById(Long id) throws SQLException, ServiceError {
        if(id==null)
            throw new IllegalArgumentException("Null id!");
        User user = userRepo.find(id);
        if(user==null)
            throw new ServiceError("User doesn't exist!");
        return user;
    }

    /**
     * updates an user's settings
     * @param id user's id
     * @param eventsNotifications if the app should notify the user about events
     * @throws SQLException
     * @throws DomainError
     * @throws RepoError
     */
    public void setUserSettings(Long id,boolean eventsNotifications) throws SQLException, DomainError, RepoError {
        if(id==null)
            throw new IllegalArgumentException("Null id!");
        User user = userRepo.find(id);
        user.getUserSettings().setReceiveEventsNotifications(eventsNotifications);
        userRepo.update(user);
    }

    /**
     * updates an user's email address
     * @param id user's id
     * @param email user's new email address
     * @throws SQLException
     * @throws DomainError
     * @throws RepoError
     */
    public void setUserEmail(Long id,String email) throws SQLException, DomainError, RepoError {
        if(id==null)
            throw new IllegalArgumentException("Null id!");
        User user = userRepo.find(id);
        user.setEmail(email);
        userRepo.update(user);
    }
}
