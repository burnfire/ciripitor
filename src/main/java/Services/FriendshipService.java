package Services;

import Domain.Abstractization.Pair;
import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Friendships.Friendship;
import Domain.Friendships.FriendshipStatus;
import Domain.Observer.Observable;
import Domain.User;
import Repository.Repository;
import Services.DTOs.FriendshipDTO;
import Repository.PageableRepository;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FriendshipService extends Observable {
    Repository<Long, User> userRepo;
    PageableRepository<Pair<Long>, Friendship> friendshipRepo;

    public FriendshipService(Repository<Long, User> userRepo, PageableRepository<Pair<Long>, Friendship> friendshipRepo) {
        this.userRepo = userRepo;
        this.friendshipRepo = friendshipRepo;
    }

    /**
     * Sends a friendship request from one user to another
     * @param senderId sender's id
     * @param receiverId receiver's id
     * @throws SQLException
     * @throws ServiceError
     * @throws DomainError
     * @throws RepoError
     */
    public void sendFriendshipRequest(Long senderId,Long receiverId) throws SQLException, ServiceError, DomainError, RepoError {
        if(!userRepo.exists(senderId) || !userRepo.exists(receiverId))
            throw new ServiceError("Users don't exist!");
        Friendship potentialFriendship = friendshipRepo.find(new Pair<>(senderId,receiverId));
        if(potentialFriendship!=null)
            if(potentialFriendship.isAccepted())
                throw new ServiceError("Users are already friends!");
            else if(potentialFriendship.getId().getLeft().equals(senderId) && potentialFriendship.getId().getRight().equals(receiverId))
                throw new ServiceError("User has already sent this friendship request!");
        Friendship friendship = new Friendship(senderId,receiverId, LocalDateTime.now(), FriendshipStatus.PENDING);
        friendshipRepo.add(friendship);
        notifyObservers();
    }

    /**
     * Accepts a friendship
     * @param senderId friendship's sender id
     * @param receiverId friendship's receiver id
     * @throws ServiceError
     * @throws SQLException
     * @throws DomainError
     * @throws RepoError
     */
    public void acceptFriendshipRequest(Long senderId,Long receiverId) throws ServiceError, SQLException, DomainError, RepoError {
        if(!userRepo.exists(senderId) || !userRepo.exists(receiverId))
            throw new ServiceError("Users don't exist!");
        Friendship friendship = friendshipRepo.find(new Pair<Long>(senderId,receiverId));
        if(friendship==null)
            throw new ServiceError("Friendship doesn't exist!");
        if(friendship.getId().getLeft().equals(receiverId) && friendship.getId().getRight().equals(senderId))
            throw new ServiceError("Sender can't accept his own friendship request!");
        friendship.acceptFriendship();
        friendshipRepo.update(friendship);
        notifyObservers();
    }

    public void rejectFriendshipRequest(Long senderId,Long receiverId) throws SQLException, ServiceError, RepoError {
        if(!userRepo.exists(senderId) || !userRepo.exists(receiverId))
            throw new ServiceError("Users don't exist!");
        Friendship friendship = friendshipRepo.find(new Pair<Long>(senderId,receiverId));
        if(friendship==null)
            throw new ServiceError("Friendship doesn't exist!");
        if(friendship.isAccepted())
            throw new ServiceError("Friendship is accepted! You have to remove it");
        friendshipRepo.remove(friendship.getId());
        notifyObservers();
    }

    /**
     * Removes a friendship, no mather who sent it
     * @param id1 first user
     * @param id2 second user
     * @throws SQLException
     * @throws ServiceError
     * @throws RepoError
     */
    public void removeFriendship(Long id1,Long id2) throws SQLException, ServiceError, RepoError {
        if(!userRepo.exists(id1) || !userRepo.exists(id2))
            throw new ServiceError("Users don't exist!");
        Friendship friendship = friendshipRepo.find(new Pair<Long>(id1,id2));
        if(friendship==null)
            throw new ServiceError("Friendship doesn't exist!");
        friendshipRepo.remove(new Pair<>(id1,id2));
        notifyObservers();
    }

    /**
     * filters friendships from a specific list of friendships
     * @param friendshipList a {@link List<Friendship>} containing friendships
     * @param condition condition to filter the friendships
     * @return a {@link List<FriendshipDTO>} containing only DTOs of friendships that match the criteria and that carry info about the initial friendships
     * @throws ServiceError
     */
    protected List<FriendshipDTO> filterFriendshipsFromList(List<Friendship> friendshipList,Predicate<Friendship> condition) throws ServiceError {
        List<FriendshipDTO> rez = friendshipList
        .stream()
            .filter(condition)
            .map(f->{
                try {
                    return new FriendshipDTO(userRepo.find(f.getId().getLeft()),userRepo.find(f.getId().getRight()),f.getDateTime(),(f.isAccepted())?FriendshipStatus.ACCEPTED:FriendshipStatus.PENDING);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    return new FriendshipDTO(null,null,LocalDateTime.now(),FriendshipStatus.PENDING);
                }
            })
            .collect(Collectors.toList());
        if(rez.stream().anyMatch(f->{return f.getSender()==null;}))
            throw new ServiceError("Can't filter users!");
        return rez;
    }

    /**
     * filters friendships based on a condition
     * @param condition the condition to filter friendships
     * @return a {@link List} containing FriendshipDTOs
     * @throws SQLException
     * @throws ServiceError
     */
    protected List<FriendshipDTO> filterFriendships(Predicate<Friendship> condition) throws SQLException, ServiceError {
        return filterFriendshipsFromList(friendshipRepo.getAll(),condition);
    }

    /**
     * gets all the friendships that match the condition and are situated on a specific page
     * @param condition condition to filter the friendships
     * @param pageNumber page's number
     * @param pageSize page's size
     * @return a {@link List<FriendshipDTO>} that contain DTOs representing the desired friendships situated on the desired page
     * @throws SQLException
     * @throws ServiceError
     */
    protected List<FriendshipDTO> filterFriendships(Predicate<Friendship> condition, int pageNumber, int pageSize) throws SQLException, ServiceError {
        List<FriendshipDTO> all = filterFriendshipsFromList(friendshipRepo.getAll(),condition);
        int startFrom = (pageNumber-1)*pageSize;
        int endTo = Integer.min(pageNumber*pageSize,all.size());
        return all.subList(startFrom,endTo);
    }

    /**
     * filters friendships bu user's id and the date (month and year)
     * @param id user's id
     * @param month friendship's month
     * @param year friendship's year
     * @return a {@link List} of FriendshipDTOs containing the desired friendships
     * @throws SQLException
     * @throws ServiceError
     */
    public List<FriendshipDTO> filterByUserDate(Long id, Month month, Year year) throws SQLException, ServiceError {
        return filterFriendships(f->{return
                f.getId().getLeft().equals(id) || f.getId().getRight().equals(id)&&
                f.getDateTime().getMonth().equals(month)
                        && f.getDateTime().getYear()==year.getValue();});
    }

    /**
     * Filters friendships that involve an user
     * @param id user's id
     * @return a {@link List<FriendshipDTO>} containing the desired users
     * @throws SQLException
     * @throws ServiceError
     */
    public List<FriendshipDTO> filterByUser(Long id) throws SQLException, ServiceError {
        return filterFriendships(f->{return
                f.getId().getLeft().equals(id) || f.getId().getRight().equals(id);});
    }

    /**
     * filters the friendships that involve an user and are situated on a specific page
     * @param id user's id
     * @param pageSize page's size
     * @param pageNumber page's number
     * @return a {@link List<FriendshipDTO>} containing the desired users
     * @throws SQLException
     * @throws ServiceError
     */
    public List<FriendshipDTO> filterByUser(Long id, int pageSize, int pageNumber) throws SQLException, ServiceError {
        return filterFriendships(f->{return
                f.getId().getLeft().equals(id) || f.getId().getRight().equals(id);},pageNumber,pageSize);
    }

    /**
     * counts the number of friends an user has
     * @param id user's id
     * @return the number of friends
     * @throws SQLException
     * @throws ServiceError
     */
    public Long countFriends(Long id) throws SQLException, ServiceError {
        return (long) filterByUser(id).stream().filter(f->{return f.getStatus()==FriendshipStatus.ACCEPTED;}).count();
    }

    public List<User> getUsersFriend(Long id) throws SQLException, ServiceError {
        List<User> rez = friendshipRepo.getAll().stream()
                .filter(f->{return f.getId().getLeft().equals(id) || f.getId().getRight().equals(id);})
                .map(f->
                {
                    if(f.getId().getLeft().equals(id)) {
                        try {
                            return userRepo.find(f.getId().getRight());
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                            return new User(-1L,"","","","","");
                        }
                    }
                    else {
                        try {
                            return userRepo.find(f.getId().getLeft());
                        } catch (SQLException throwables) {
                            throwables.printStackTrace();
                        }
                    }
                    return null;
                }).collect(Collectors.toList());
        if(rez.stream().anyMatch(u->{return u==null||u.getId().equals(-1L);}))
            throw new ServiceError("Can't acquire friends!");
        return rez;
    }

    /**
     * returns the friendship havind 2 ids
     * @param id1 first id
     * @param id2 second id
     * @return a {@link Friendship} having the two ids in either order
     * @throws SQLException
     */
    public Friendship findFriendship(Long id1,Long id2) throws SQLException {
        return friendshipRepo.find(new Pair<>(id1,id2));
    }
}
