package Services;

import Domain.Friendships.FriendshipStatus;
import Domain.Profiles.PrivateProfile;
import Domain.Profiles.PublicProfile;
import Domain.User;
import Services.DTOs.ActivityDTO;
import Services.DTOs.MessageDTO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ActivityService {
    PrivateProfile loggedInUserProfile;
    PublicProfile otherUserProfile;

    public ActivityService(PrivateProfile loggedInUserProfile, PublicProfile otherUserProfile) {
        this.loggedInUserProfile = loggedInUserProfile;
        this.otherUserProfile = otherUserProfile;
    }

    /**
     * gets all messages sent by an user and received either by everyone (if no other user is selected) or between two users (if the other user is selected)
     *
     * @return a {@link Stream<ActivityDTO>} containing all activities regarding messages
     */
    Stream<ActivityDTO> getMessages() {
        Stream<ActivityDTO> activityStream;
        Stream<MessageDTO> messageStream;
        messageStream = loggedInUserProfile.getMessages().stream();
        if (otherUserProfile.getOwner() != null) {
            messageStream = messageStream.filter(m ->
            {
                return m.getReceivers().contains(otherUserProfile.getOwner());
            });
        }
        activityStream = messageStream.map(m ->
        {
            StringBuilder description;
            if (m.getReceivers().contains(loggedInUserProfile.getOwner()))
                description = new StringBuilder(String.format("%s has received a message from %s %s(%s): %s",
                        loggedInUserProfile.getOwner().getNickname(),
                        m.getSender().getFirstName(),
                        m.getSender().getLastName(),
                        m.getSender().getNickname(),
                        m.getMessage()));
            else {
                description = new StringBuilder(String.format("%s has send a message to ", loggedInUserProfile.getOwner().getNickname()));
                for(User u:m.getReceivers())
                {
                    description.append(String.format("%s %s(%s), ", u.getFirstName(), u.getLastName(), u.getNickname()));
                }
                description.deleteCharAt(description.lastIndexOf(", "));
                description.append(String.format(": %s",m.getMessage()));
            }

            return new ActivityDTO(description.toString(), m.getDatetime());
        });
        return activityStream;
    }

    /**
     * filters activity DTOs, keeping only those between two dates
     * @param activityStream a {@link Stream<ActivityDTO>} containing the desired activities to filter
     * @param startDate the start date to filter
     * @param endDate the end date to filter
     * @return a {@link Stream<ActivityDTO>} containing only the activities that have their dates between {@param startDate} and {@param endDate}
     */
    Stream<ActivityDTO> filterByDate(Stream<ActivityDTO> activityStream, LocalDateTime startDate, LocalDateTime endDate)
    {
        return activityStream.filter(a->
        {
            return a.getDateTime().isAfter(startDate) && a.getDateTime().isBefore(endDate);
        });
    }

    /**
     * same as getMessages with no parameter, but gets the messages only between two dates
     *
     * @param startDate start date of the messages
     * @param endDate   end date of the messages
     * @return same as getMessages with no parameters, but gets only the activities that has the dates between the two parameters
     */
    public List<ActivityDTO> getMessages(LocalDateTime startDate, LocalDateTime endDate) {
        return filterByDate(getMessages(),startDate,endDate)
                .collect(Collectors.toList());
    }

    /**
     * gets the friendship activities that involve the logged in user
     * @return a {@link Stream<ActivityDTO>} containing all friendship activities involving the logged in user
     */
    Stream<ActivityDTO> getFriendships()
    {
        return loggedInUserProfile.getFriends().stream()
                .map(f->
                {
                    String description;
                    if(f.getStatus()== FriendshipStatus.PENDING)
                    {
                        if(f.getSender().equals(loggedInUserProfile.getOwner()))
                            description=String.format("%s has sent a friendship request to %s %s(%s) at %s",
                                    loggedInUserProfile.getOwner().getNickname(),
                                    f.getReceiver().getFirstName(),
                                    f.getReceiver().getLastName(),
                                    f.getReceiver().getNickname(),
                                    f.getDatetime().toString());
                        else
                            description=String.format("%s has received a friendship request from %s %s(%s) at %s",
                                    f.getReceiver().getNickname(),
                                    f.getSender().getFirstName(),
                                    f.getSender().getLastName(),
                                    f.getSender().getNickname(),
                                    f.getDatetime().toString());
                    }
                    else
                    {
                        description = String.format("%s and %s %s(%s) are friends since %s",
                                f.getSender().getNickname(),
                                f.getReceiver().getFirstName(),
                                f.getReceiver().getLastName(),
                                f.getReceiver().getNickname(),
                                f.getDatetime().toString());
                    }

                    return new ActivityDTO(description,f.getDatetime());
                });
    }

    /**
     * filters friendships activities by their dates
     * @param startDate the beginning of the date interval
     * @param endDate the ending of the date interval
     * @return a {@link List<ActivityDTO>} containing friendship activities between {@param startDate} and {@param endDate}
     */
    public List<ActivityDTO> getFriendships(LocalDateTime startDate, LocalDateTime endDate)
    {
        return filterByDate(getFriendships(),startDate,endDate).collect(Collectors.toList());
    }
}
