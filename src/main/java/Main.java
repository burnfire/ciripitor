import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import UI.GUI.GUI;
import UI.UI;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws SQLException, RepoError, DomainError, ServiceError, IOException {
        //UI
        UI ui;
        ui=new GUI();
        ui.run();
    }
}
