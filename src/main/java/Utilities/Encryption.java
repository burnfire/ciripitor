package Utilities;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public interface Encryption {
    SecretKeySpec encryptionKey = getKey("SECRETDISCRET");

    /**
     * gets the encryption key based on a string
     * @param keyString string base for the encryption key
     * @return a {@link SecretKeySpec} that represents the encryption key
     */
    static SecretKeySpec getKey(String keyString){
        byte[] bytes = keyString.getBytes(StandardCharsets.UTF_8);
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-1");
            bytes = sha.digest(bytes);
            bytes = Arrays.copyOf(bytes,16);
            return new SecretKeySpec(bytes,"AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * encrypts a string
     * @param string the string to encrypt
     * @return a {@link String} that contains a Base64 encrypted version of the initial string
     */
    default String encryptString(String string){
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE,encryptionKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(string.getBytes(StandardCharsets.UTF_8)));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }
    default String decryptString(String cryptedString)
    {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE,encryptionKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(cryptedString.getBytes(StandardCharsets.UTF_8))));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            e.printStackTrace();
        }
        return "";
    }
}
