package Domain.Validators;

import Domain.Errors.DomainError;
import Domain.Events.Event;

import java.time.LocalDateTime;
import java.time.temporal.*;

public class EventValidator implements Validator<Event>{
    @Override
    public void validate(Event entity) throws DomainError {
        if(entity==null)
            throw new DomainError("Null entity!");
        if(entity.getStartDate()==null)
            throw new DomainError("Null start date!");
        if(entity.getEndDate()==null)
            throw new DomainError("Null end date!");
        if(entity.getAttendees()==null)
            throw new DomainError("Null list of attendees!");
        /*if(ChronoUnit.MINUTES.between(LocalDateTime.now(),entity.getStartDate())<5)
        {
            throw new DomainError("Events must be created with more than 5 minutes before start date!");
        }*/
        if(ChronoUnit.SECONDS.between(entity.getStartDate(),entity.getEndDate())<0)
            throw new DomainError("Start date must be before the end date!");
        if(ChronoUnit.MINUTES.between(entity.getStartDate(),entity.getEndDate())<5)
        {
            throw new DomainError("Events must last at least 5 minutes!");
        }
        if(entity.getTitle()==null || entity.getTitle().isEmpty())
            throw new DomainError("Empty or null title!");
    }
}
