package Domain.Validators;

import Domain.Errors.DomainError;
import Domain.User;

import java.util.regex.Pattern;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws DomainError {
        Pattern namePattern = Pattern.compile("^[a-zA-Z ]+$");
        Pattern nicknamePattern = Pattern.compile("^[a-zA-Z0-9.!]{3,}$");
        Pattern passwordPattern = Pattern.compile("^[a-zA-Z0-9.!@#$%^&*(){}+=_|:]{3,}");
        Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9]{3,}@[a-z]{2,}.[a-z]{2,5}$");
        String res="";
        if(entity==null)
            throw new DomainError("Null entity!");
        if(entity.getId()==null)
            throw new DomainError("Null id!");
        if(!namePattern.matcher(entity.getFirstName()).matches())
            res+="Invalid first name!\n";
        if(!namePattern.matcher(entity.getLastName()).matches())
            res+="Invalid last name!\n";
        if(!nicknamePattern.matcher(entity.getNickname()).matches())
            res+="Invalid nickname!\n";
        if(!passwordPattern.matcher(entity.getPasswd()).matches())
            res+="Invalid password!\n";
        if(!emailPattern.matcher(entity.getEmail()).matches() && !entity.getEmail().isEmpty())
            res+="Invalid email address!\n";
        if(entity.getId()<0)
            res+="Invalid id!\n";
        if(!res.isEmpty())
            throw new DomainError(res);
    }
}
