package Domain.Validators;

import Domain.Errors.DomainError;

@FunctionalInterface
public interface Validator<E> {
    void validate(E entity) throws DomainError;
}
