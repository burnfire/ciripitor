package Domain.Validators;

import Domain.Errors.DomainError;
import Domain.Friendships.Friendship;

public class FriendshipValidator implements Validator<Friendship>{
    @Override
    public void validate(Friendship entity) throws DomainError {
        if(entity.getId().getLeft()<0 || entity.getId().getRight()<0)
            throw new DomainError("IDs can't be negative!");
        if(entity.getId().getLeft().equals(entity.getId().getRight()))
            throw new DomainError("User can't friend himself!");
    }
}
