package Domain.Validators;

import Domain.Errors.DomainError;
import Domain.Message;

public class MessageValidator implements Validator<Message>{
    @Override
    public void validate(Message entity) throws DomainError {
        String res="";
        if(entity==null)
            throw new DomainError("Null entity!");
        if(entity.getId()==null)
            throw new DomainError("Null id!");
        if(entity.getFrom()<0)
            res+="Sender's id can't be negative!";
        if(entity.getTo().stream().anyMatch(i->{return i<0;}))
            res+="Receiver's id can't be negative!";
        if(entity.getTo().stream().anyMatch(i->{return i.equals(entity.getFrom());}))
            res+="User can't message himself!";
        if(entity.getTo().size()==0)
            res+="No receivers!";
        if(!res.isEmpty())
            throw new DomainError(res);
    }
}
