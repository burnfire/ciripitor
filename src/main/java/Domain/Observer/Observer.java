package Domain.Observer;

import Domain.Errors.ServiceError;

import java.sql.SQLException;

public interface Observer {

    /**
     * subscribes to an {@link Observable}
     * @param observable observable to subscribe to
     */
    default void subscribe(Observable observable)
    {
        observable.addSubscriber(this);
    }

    /**
     * removes the subscription from the observable
     * @param observable - observable to remove this Observer's subscription from
     */
    default void unsubscribe(Observable observable)
    {
        observable.removeSubscriber(this);
    }

    /**
     * updates the status of this Observer
     */
    void update() throws SQLException, ServiceError;
}
