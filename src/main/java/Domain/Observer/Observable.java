package Domain.Observer;

import Domain.Errors.ServiceError;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class Observable {
    Set<Observer> observers=new HashSet<>();

    /**
     * adds the observer to the set if it doesn't exist already
     * @param observer observer to add
     */
    public void addSubscriber(Observer observer)
    {
        observers.add(observer);
    }

    /**
     * removes an observer from observer's list
     * @param observer the observer to remove
     */
    public void removeSubscriber(Observer observer)
    {
        observers.remove(observer);
    }

    /**
     * notifies all observers subscribed to this {@link Observable}
     */
    public void notifyObservers() throws SQLException, ServiceError {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
