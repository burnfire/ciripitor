package Domain;

import Domain.Abstractization.Entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message extends Entity<Long> {
    String message;
    Long from;
    List<Long> to;
    LocalDateTime dateTime;

    public Message(String message, Long from, List<Long> to, LocalDateTime dateTime) {
        this.message = message;
        this.from = from;
        this.to = to;
        this.dateTime = dateTime;
    }

    public Message(Long id,String message, Long from, List<Long> to, LocalDateTime dateTime) {
        this.message = message;
        this.from = from;
        this.to = to;
        this.dateTime = dateTime;
        setId(id);
    }

    public Message(Long id,String message, Long from, Long to, LocalDateTime dateTime) {
        this.message = message;
        this.from = from;
        this.to = new ArrayList<>();
        this.to.add(to);
        this.dateTime = dateTime;
        setId(id);
    }

    public String getMessage() {
        return message;
    }

    public Long getFrom() {
        return from;
    }

    public List<Long> getTo() {
        return to;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
