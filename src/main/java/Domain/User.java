package Domain;

import Domain.Abstractization.Entity;

public class User extends Entity<Long> {
    String firstName,lastName,nickname,passwd,email;
    UserSettings userSettings;
    public static class UserSettings
    {
        boolean receiveEventsNotifications;

        public UserSettings(boolean receiveEventsNotifications) {
            this.receiveEventsNotifications = receiveEventsNotifications;
        }

        public boolean isReceiveEventsNotifications() {
            return receiveEventsNotifications;
        }

        public void setReceiveEventsNotifications(boolean receiveEventsNotifications) {
            this.receiveEventsNotifications = receiveEventsNotifications;
        }
    }

    public User(Long id, String firstName, String lastName, String nickname, String passwd,String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
        this.passwd = passwd;
        this.email=email;
        setId(id);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public UserSettings getUserSettings() {
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
