package Domain.Friendships;

import Domain.Abstractization.Entity;
import Domain.Abstractization.Pair;

import java.time.LocalDateTime;

public class Friendship extends Entity<Pair<Long>> {
    LocalDateTime dateTime;
    FriendshipStatus status = FriendshipStatus.PENDING;

    public Friendship(Long sender, Long receiver, LocalDateTime dateTime, FriendshipStatus status) {
        setId(new Pair<Long>(sender,receiver));
        this.dateTime = dateTime;
        this.status = status;
    }

    public Friendship(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * accepts the friendships between users
     */
    public void acceptFriendship()
    {
        status= FriendshipStatus.ACCEPTED;
    }

    /**
     * checks if the friendship is accepted
     * @return true if the friendship is accepted, false otherwise
     */
    public boolean isAccepted()
    {
        return status== FriendshipStatus.ACCEPTED;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setStatus(FriendshipStatus status) {
        this.status = status;
    }
}
