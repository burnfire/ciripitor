package Domain.Abstractization;

public class Pair<A>{
    A left,right;

    public Pair(A left, A right) {
        this.left = left;
        this.right = right;
    }

    public A getLeft() {
        return left;
    }

    public A getRight() {
        return right;
    }
}
