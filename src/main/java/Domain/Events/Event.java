package Domain.Events;

import Domain.Abstractization.Entity;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class Event extends Entity<Long> {
    String title;
    String description;
    LocalDateTime startDate;
    LocalDateTime endDate;
    Long creatorId;
    List<Long> attendees;

    public Event(Long id, String title, String description, LocalDateTime startDate, LocalDateTime endDate, Long creatorId, List<Long> attendees) {
        setId(id);
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.creatorId = creatorId;
        this.attendees=attendees;
    }

    public Event(String title, String description, LocalDateTime startDate, LocalDateTime endDate, Long creatorId, List<Long> attendees) {
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.creatorId = creatorId;
        this.attendees = attendees;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Long getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Long creatorId) {
        this.creatorId = creatorId;
    }

    public List<Long> getAttendees() {
        return attendees;
    }

    public void setAttendees(List<Long> attendees) {
        this.attendees = attendees;
    }

    public void addAttendee(Long attendee)
    {
        this.attendees.add(attendee);
    }

    public void removeAttendee(Long attendee)
    {
        this.attendees.remove(attendee);
    }

    public boolean isStarted()
    {
        return LocalDateTime.now().isAfter(startDate);
    }

    public boolean isOngoing()
    {
        return LocalDateTime.now().isBefore(endDate) && LocalDateTime.now().isAfter(startDate);
    }

    public boolean isEnded()
    {
        return LocalDateTime.now().isAfter(endDate);
    }
}
