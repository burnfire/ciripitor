package Domain.Events;

import Domain.Errors.ServiceError;
import Domain.Observer.Observable;
import Domain.Observer.Observer;
import Domain.User;
import Services.EventService;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.util.Duration;
import javafx.util.Pair;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class EventsHandler extends Observable implements Observer {
    List<Event> allEvents=new ArrayList<>();
    List<Pair<Event, EventNotifyTime>> alreadyNotified=new ArrayList<>();
    Queue<Pair<Event, EventNotifyTime>> upcomingEvents=new LinkedList<>();
    User currentUser;
    Timeline eventsCheck;
    static final EventNotifyTime[] notifyTimes =
            {
                    new EventNotifyTime(1L,ChronoUnit.MINUTES),
                    new EventNotifyTime(5L,ChronoUnit.MINUTES),
                    new EventNotifyTime(1L,ChronoUnit.HOURS),
                    new EventNotifyTime(2L,ChronoUnit.HOURS),
                    new EventNotifyTime(1L,ChronoUnit.DAYS)
            };

    //Services
    EventService eventService;

    //Local classes

    public List<Event> getAllEvents() {
        return allEvents;
    }

    /**
     * stops the event checks timer
     */
    public void stopEventChecks()
    {
        eventsCheck.stop();
        //eventsCheck.cancel();
    }

    public EventsHandler(User currentUser, EventService eventService) throws SQLException, ServiceError {
        this.currentUser = currentUser;
        this.eventService = eventService;
        this.eventService.addSubscriber(this);
        this.eventsCheck = new Timeline(new KeyFrame(Duration.minutes(0.5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (allEvents != null) {
                    for (Event e : allEvents) {
                        if (!e.isStarted()) {
                            int i=0;
                            for (i=0;i< notifyTimes.length;i++) {
                                EventNotifyTime time = notifyTimes[i];
                                if (!alreadyNotified.contains(new Pair<>(e, time)) && time.getUnit().between(LocalDateTime.now(), e.getStartDate()) < time.getTimeUnits()) {
                                    upcomingEvents.add(new Pair<>(e, time));
                                    alreadyNotified.add(new Pair<>(e, time));
                                    break;
                                }
                            }
                            for(;i< notifyTimes.length;i++)
                            {
                                Pair<Event,EventNotifyTime> pair = new Pair<>(e,notifyTimes[i]);
                                if(!alreadyNotified.contains(pair))
                                    alreadyNotified.add(pair);
                            }
                        }
                    }
                }
                try {
                    notifyObservers();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (ServiceError serviceError) {
                    serviceError.printStackTrace();
                }
            }
        }));
        eventsCheck.setCycleCount(Timeline.INDEFINITE);
        eventsCheck.play();
        //eventsCheck.schedule(new EventChecker(), 0, 1000 * 60);
        update();
    }

    public void setCurrentUser(User currentUser) throws SQLException, ServiceError {
        this.currentUser = currentUser;
        update();
        eventsCheck.play();
    }

    /**
     * updates the status of this Observer
     */
    @Override
    public void update() throws SQLException, ServiceError {
        alreadyNotified.clear();
        upcomingEvents.clear();
        if (currentUser != null)
            allEvents = eventService.getUserEvents(currentUser.getId());
        else
            allEvents.clear();
        notifyObservers();
    }

    public Queue<Pair<Event, EventNotifyTime>> getUpcomingEvents() {
        return upcomingEvents;
    }
}
