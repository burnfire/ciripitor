package Domain.Events;

import java.time.temporal.ChronoUnit;

public class EventNotifyTime {
    Long timeUnits;
    ChronoUnit unit;

    public EventNotifyTime(Long timeUnits, ChronoUnit unit) {
        this.timeUnits = timeUnits;
        this.unit = unit;
    }

    public Long getTimeUnits() {
        return timeUnits;
    }

    public void setTimeUnits(Long timeUnits) {
        this.timeUnits = timeUnits;
    }

    public ChronoUnit getUnit() {
        return unit;
    }

    public void setUnit(ChronoUnit unit) {
        this.unit = unit;
    }
}
