package Domain;

import java.time.LocalDateTime;
import java.util.List;

public class ReplyMessage extends Message {
    Long replyTo;

    public ReplyMessage(String message, Long from, List<Long> to, LocalDateTime dateTime, Long replyTo) {
        super(message, from, to, dateTime);
        this.replyTo = replyTo;
    }

    public ReplyMessage(Long replyTo, Long id, String message, Long from, Long to, LocalDateTime dateTime) {
        super(id, message, from, to, dateTime);
        this.replyTo=replyTo;
    }

    public Long getReplyTo() {
        return replyTo;
    }
}
