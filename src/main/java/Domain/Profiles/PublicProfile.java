package Domain.Profiles;

import Domain.Errors.ServiceError;
import Domain.Friendships.Friendship;
import Domain.User;
import Services.FriendshipService;
import Services.UserService;

import java.sql.SQLException;

public class PublicProfile extends Profile{
    User loggedUser;//logged in user
    Friendship friendshipBetweenUsers;//friendship between logged in user and this profile's owner

    //Services
    FriendshipService friendshipService;
    public PublicProfile(User owner, User loggedUser, UserService userService,FriendshipService friendshipService) throws SQLException, ServiceError {
        super(owner, userService);
        this.friendshipService=friendshipService;
        this.loggedUser=loggedUser;
        update();
    }

    public void setLoggedUser(User loggedUser) throws SQLException, ServiceError {
        this.loggedUser = loggedUser;
        update();
    }

    public User getLoggedUser() {
        return loggedUser;
    }

    public Friendship getFriendshipBetweenUsers() {
        return friendshipBetweenUsers;
    }

    /**
     * updates the status of this Observer
     */
    @Override
    public void update() throws SQLException, ServiceError {
        if(owner!=null && loggedUser!=null)
        {
            friendshipBetweenUsers=friendshipService.findFriendship(loggedUser.getId(), owner.getId());
            notifyObservers();
        }
    }
}
