package Domain.Profiles;

import Domain.Errors.ServiceError;
import Domain.Events.Event;
import Domain.Events.EventsHandler;
import Domain.Observer.Observer;
import Domain.User;
import Services.DTOs.FriendshipDTO;
import Services.DTOs.MessageDTO;
import Services.EventService;
import Services.FriendshipService;
import Services.MessageService;
import Services.UserService;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PrivateProfile extends Profile implements Observer {
    List<MessageDTO> messages;
    Set<User> chattedUsers;//Users that profile owner has chatted with
    List<FriendshipDTO> friends;
    List<Event> createdEvents;
    EventsHandler eventsHandler;
    //Services
    MessageService messageService;
    FriendshipService friendshipService;
    EventService eventService;
    public PrivateProfile(User user, UserService userService, FriendshipService friendshipService, MessageService messageService, EventService eventService) throws SQLException, ServiceError {
        super(user, userService);
        this.messageService=messageService;
        this.friendshipService=friendshipService;
        this.eventsHandler=new EventsHandler(user,eventService);
        this.eventsHandler.addSubscriber(this);
        this.eventService=eventService;
        update();
    }

    public List<MessageDTO> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDTO> messages) throws SQLException, ServiceError {
        this.messages = messages;
        update();
    }

    public List<FriendshipDTO> getFriends() {
        return friends;
    }

    /**
     * updates the status of this Observer
     */
    @Override
    public void update() throws SQLException, ServiceError {
        if(owner!=null)
        {
            chattedUsers = messageService.getUsersThatChattedWithUser(owner.getId());
            messages = messageService.getMessagesFromUser(owner.getId());
            friends = friendshipService.filterByUser(owner.getId());
            createdEvents = eventService.getEventsUserCreated(owner.getId());
            owner = userService.getUserById(owner.getId());
            notifyObservers();
        }
    }

    public List<Event> getCreatedEvents() {
        return createdEvents;
    }

    public EventsHandler getEventsHandler() {
        return eventsHandler;
    }
}
