package Domain.Profiles;

import Domain.Errors.ServiceError;
import Domain.User;
import Services.EventService;
import Services.FriendshipService;
import Services.MessageService;
import Services.UserService;

import java.io.InvalidObjectException;
import java.sql.SQLException;

public class ProfileFactory {

    private final static ProfileFactory instance=new ProfileFactory();

    //Services
    UserService userService=null;
    FriendshipService friendshipService=null;
    MessageService messageService=null;
    EventService eventService=null;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setFriendshipService(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    private ProfileFactory(){}

    public static ProfileFactory getInstance() {
        return instance;
    }

    /**
     * checks if the services are set
     * @return true if all are set. False otherwise
     */
    private boolean checkServicesSet()
    {
        return !(userService==null || friendshipService==null || messageService==null || eventService==null);
    }

    /**
     * creates a {@link PublicProfile} that contains info that a general user can see about a particular user
     * it also subscribes to UserService and FriendshipService
     * @param owner profile's owner
     * @return a {@link PublicProfile} that satisfies the description
     * @throws SQLException
     * @throws ServiceError
     * @throws InvalidObjectException if services are not set for this factory
     */
    public PublicProfile getPublicProfile(User owner,User loggedUser) throws SQLException, ServiceError, InvalidObjectException {
        if(!checkServicesSet())
            throw new InvalidObjectException("Services are not set!");
        PublicProfile profile = new PublicProfile(owner,loggedUser,userService,friendshipService);
        userService.addSubscriber(profile);
        friendshipService.addSubscriber(profile);
        return profile;
    }

    public PrivateProfile getPrivateProfile(User owner) throws InvalidObjectException, SQLException, ServiceError {
        if(!checkServicesSet())
            throw new InvalidObjectException("Services are not set!");
        PrivateProfile privateProfile = new PrivateProfile(owner,userService,friendshipService,messageService,eventService);
        userService.addSubscriber(privateProfile);
        friendshipService.addSubscriber(privateProfile);
        messageService.addSubscriber(privateProfile);
        eventService.addSubscriber(privateProfile);
        return privateProfile;
    }
}
