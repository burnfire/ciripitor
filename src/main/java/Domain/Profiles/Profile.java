package Domain.Profiles;

import Domain.Errors.ServiceError;
import Domain.Observer.Observable;
import Domain.Observer.Observer;
import Domain.User;
import Services.UserService;

import java.sql.SQLException;

public abstract class Profile extends Observable implements Observer {
    User owner;//profile's owner

    UserService userService;

    public Profile(User owner, UserService userService) {
        this.owner = owner;
        this.userService=userService;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) throws SQLException, ServiceError {
        this.owner = owner;
        update();
    }
}
