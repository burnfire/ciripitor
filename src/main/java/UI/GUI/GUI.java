package UI.GUI;

import Domain.Abstractization.Pair;
import Domain.Events.Event;
import Domain.Friendships.Friendship;
import Domain.Message;
import Domain.Profiles.ProfileFactory;
import Domain.User;
import Domain.Validators.EventValidator;
import Domain.Validators.FriendshipValidator;
import Domain.Validators.MessageValidator;
import Domain.Validators.UserValidator;
import Repository.FriendshipDBRepository;
import Repository.Repository;
import Repository.PageableRepository;
import Repository.UserDBRepository;
import Repository.MessageDBRepository;
import Repository.EventDBRepository;
import Services.EventService;
import Services.FriendshipService;
import Services.MessageService;
import Services.UserService;
import UI.GUI.Controlers.*;
import UI.UI;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class GUI extends Application implements UI{
    @FXML
    LoginController loginController;
    @FXML
    RegisterController registerController;
    @FXML
    HomeController homeController;
    @FXML
    ActivityController activityController;

    //Repositories
    Repository<Long, User> userRepo;
    PageableRepository<Pair<Long>, Friendship> friendshipRepo;
    Repository<Long, Message> messageRepo;
    Repository<Long, Event> eventRepo;
    //Services
    UserService userService;
    FriendshipService friendshipService;
    MessageService messageService;
    EventService eventService;

    Map<String,Scene> scenes = new HashMap<>();
    Stage primaryStage;
    FXMLLoader fxmlLoader;
    public GUI() throws IOException, SQLException {
        //sceneSetter=new SceneSetter();
        //Repos
        Connection conexiune = DriverManager.getConnection("jdbc:postgresql://localhost:5432/socialnetwork","postgres","12345678");
        userRepo = new UserDBRepository(new UserValidator(),conexiune);
        friendshipRepo = new FriendshipDBRepository(new FriendshipValidator(),conexiune);
        messageRepo = new MessageDBRepository(new MessageValidator(),conexiune);
        eventRepo = new EventDBRepository(new EventValidator(),conexiune);

        //Services
        userService=new UserService(userRepo);
        friendshipService=new FriendshipService(userRepo, friendshipRepo);
        messageService=new MessageService(userRepo,messageRepo);
        eventService = new EventService(eventRepo,userRepo);

        //Factories
        //Profile Factory
        ProfileFactory.getInstance().setUserService(userService);
        ProfileFactory.getInstance().setFriendshipService(friendshipService);
        ProfileFactory.getInstance().setMessageService(messageService);
        ProfileFactory.getInstance().setEventService(eventService);
    }

    /**
     * loads in memory all scenes for this window
     * @throws IOException
     */
    void loadScenesControllers() throws IOException {
        AnchorPane anchorPane;
        VBox vBox;
        HBox hBox;

        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/registerView.fxml"));
        hBox = fxmlLoader.load();
        scenes.put("Register",new Scene(hBox));
        registerController = fxmlLoader.getController();
        registerController.setUserService(userService);
        registerController.setGui(this);

        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/homeView.fxml"));
        vBox = fxmlLoader.load();
        scenes.put("Home",new Scene(vBox));
        homeController = fxmlLoader.getController();
        homeController.setUserService(userService);
        homeController.setFriendshipService(friendshipService);
        homeController.setMessageService(messageService);
        homeController.setEventService(eventService);
        homeController.setGui(this);

        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/loginView.fxml"));
        //anchorPane = fxmlLoader.load();
        hBox = fxmlLoader.load();
        scenes.put("Login",new Scene(hBox));
        loginController = fxmlLoader.getController();
        loginController.setUserService(userService);
        loginController.setGui(this);
        loginController.setHomeController(homeController);

        fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/activityView.fxml"));
        anchorPane = fxmlLoader.load();
        scenes.put("Activities",new Scene(anchorPane));
        activityController = fxmlLoader.getController();
        activityController.setGui(this);
    }

    public ActivityController getActivityController() {
        return activityController;
    }

    /**
     * sets the current scene
     * @param sceneName scene's name
     */
    public void setScene(String sceneName)
    {
        primaryStage.setScene(scenes.get(sceneName));
    }
    @Override
    public void run() {
        launch();
    }

    /**
     * The main entry point for all JavaFX applications.
     * The start method is called after the init method has returned,
     * and after the system is ready for the application to begin running.
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @param primaryStage the primary stage for this application, onto which
     *                     the application scene can be set.
     *                     Applications may create other stages, if needed, but they will not be
     *                     primary stages.
     * @throws Exception if something goes wrong
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        //primaryStage.setScene(sceneSetter.getScene("Login"));
        loadScenesControllers();
        this.primaryStage=primaryStage;
        setScene("Login");
        primaryStage.show();
    }

    /**
     * This method is called when the application should stop, and provides a
     * convenient place to prepare for application exit and destroy resources.
     *
     * <p>
     * The implementation of this method provided by the Application class does nothing.
     * </p>
     *
     * <p>
     * NOTE: This method is called on the JavaFX Application Thread.
     * </p>
     *
     * @throws Exception if something goes wrong
     */
    @Override
    public void stop() throws Exception {
        super.stop();
        this.homeController.getCurrentUser().getEventsHandler().stopEventChecks();
    }
}
