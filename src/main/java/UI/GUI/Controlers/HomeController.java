package UI.GUI.Controlers;

import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Errors.UIError;
import Domain.Events.Event;
import Domain.Events.EventNotifyTime;
import Domain.Friendships.Friendship;
import Domain.Friendships.FriendshipStatus;
import Domain.Message;
import Domain.Observer.Observer;
import Domain.Profiles.PrivateProfile;
import Domain.Profiles.ProfileFactory;
import Domain.Profiles.PublicProfile;
import Domain.User;
import Services.DTOs.FriendshipDTO;
import Services.DTOs.MessageDTO;
import Services.EventService;
import Services.FriendshipService;
import Services.MessageService;
import Services.UserService;
import UI.DateTimeFormats;
import UI.GUI.GUI;
import Utilities.Encryption;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HomeController implements Observer, DateTimeFormats {

    //Services
    UserService userService;
    FriendshipService friendshipService;
    MessageService messageService;
    EventService eventService;
    //GUI (scene manager)
    GUI gui;
    //Profiles
    PrivateProfile currentUser;
    PublicProfile searchedUser;
    //Constants
    private static final int pageSize = 5;

    //Local classes
    public static class EventsTableEntry
    {
        StringProperty eventId;
        StringProperty eventTitle;

        public EventsTableEntry(StringProperty eventId, StringProperty eventTitle) {
            this.eventId = eventId;
            this.eventTitle = eventTitle;
        }

        public String getEventId() {
            return eventId.get();
        }

        public StringProperty eventIdProperty() {
            return eventId;
        }

        public void setEventId(String eventId) {
            this.eventId.set(eventId);
        }

        public String getEventTitle() {
            return eventTitle.get();
        }

        public StringProperty eventTitleProperty() {
            return eventTitle;
        }

        public void setEventTitle(String eventTitle) {
            this.eventTitle.set(eventTitle);
        }
    }

    @FXML
    ComboBox<String> nicknameSearchInput;
    @FXML
    Label friendshipStatusLabel;
    @FXML
    Button friendshipActionButton;
    @FXML
    TabPane mainTabPane;
    @FXML
    ListView<String> friendsList;
    @FXML
    Button sendMessageButton;
    @FXML
    ListView<String> receiversList;
    @FXML
    Button sendSearchedUserMessageButton;
    @FXML
    ListView<MessageDTO> messagesList;
    @FXML
    TextField messageInput;
    @FXML
    PasswordField changeinfoCurrentPasswordInput;
    @FXML
    PasswordField changeinfoNewPasswordInput;
    @FXML
    TextField changeinfoNewFirstNameInput;
    @FXML
    TextField changeinfoNewLastNameInput;
    @FXML
    TextField changeinfoNewNicknameInput;
    @FXML
    Button changeinfoSaveButton;
    @FXML
    TextField eventTitleInput;
    @FXML
    TextArea eventDescriptionInput;
    @FXML
    DatePicker eventStartDatePicker;
    @FXML
    TextField eventStartTimePicker;
    @FXML
    DatePicker eventEndDatePicker;
    @FXML
    TextField eventEndTimePicker;
    @FXML
    TableView<EventsTableEntry> ownEventsTable;
    @FXML
    Button removeEventButton;
    @FXML
    TableView<EventsTableEntry> allEventsTable;
    @FXML
    Button checkNotificationsButton;
    @FXML
    CheckBox eventsNotificationsCheckBox;
    @FXML
    Pagination friendsListPagination;
    @FXML
    TextField changeinfoNewEmail;
    @FXML
    Label selfNameLabel;
    @FXML
    Label selfEmailLabel;
    @FXML
    Label selfFriendsCountLabel;
    @FXML
    Label selfChatsCountLabel;
    @FXML
    Label selfCreatedEventsLabel;
    @FXML
    Label searchedUserNameLabel;

    public HomeController() {

    }

    public void setMessageService(MessageService messageService) {
        this.messageService = messageService;
    }

    public void setCurrentUser(User currentUser) throws SQLException, ServiceError, InvalidObjectException {
        this.currentUser.setOwner(currentUser);
        this.searchedUser.setLoggedUser(currentUser);
        this.currentUser.notifyObservers();
        this.currentUser.getEventsHandler().setCurrentUser(currentUser);
        this.currentUser.getEventsHandler().update();
        update();
    }

    public void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    private void updateProfile(){
        selfNameLabel.setText(String.format("%s %s (%s)",
                currentUser.getOwner().getFirstName(),
                currentUser.getOwner().getLastName(),
                currentUser.getOwner().getNickname()));
        updateProfileInfo();
    }

    private void updateProfileInfo() {
        updateProfileEmail();
        updateProfileFriendsCount();
        updateProfileChatsCount();
        updateProfileCreatedEventsCount();
    }

    private void updateProfileCreatedEventsCount() {
        int eventsCount = currentUser.getCreatedEvents().size();
        String text="";
        if(eventsCount==0)
            text="No events created";
        else if(eventsCount==1)
            text="One event created";
        else
            text=String.format("%d events created",eventsCount);
        selfCreatedEventsLabel.setText(text);
    }

    private void updateProfileChatsCount() {
        int chatsCount = currentUser.getMessages().size();
        String text="";
        if(chatsCount==0)
            text="No chats";
        else if(chatsCount==1)
            text="One chat";
        else
            text=String.format("%d chats",chatsCount);
        selfChatsCountLabel.setText(text);
    }

    private void updateProfileFriendsCount() {
        int friendsCount = currentUser.getFriends().size();
        if(friendsCount==0)
            selfFriendsCountLabel.setText("No friends");
        else if(friendsCount==1)
            selfFriendsCountLabel.setText("One friend");
        else
            selfFriendsCountLabel.setText(String.format("%d friends",friendsCount));
    }

    private void updateProfileEmail() {
        String email = currentUser.getOwner().getEmail();
        selfEmailLabel.setText((email.isEmpty())?"No email set":email);
    }

    private void updateChats() {
        try {
            Set<User> users = messageService.getUsersThatChattedWithUser(currentUser.getOwner().getId());
            receiversList.getItems().clear();
            users.forEach(u -> {
                receiversList.getItems().add(u.getNickname());
            });
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setFriendshipService(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
        //Set cell value factories for tables
        ownEventsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("eventId"));
        ownEventsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("eventTitle"));
        allEventsTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("eventId"));
        allEventsTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("eventTitle"));
        //Set cell factories for tables
        receiversList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        try {
            this.currentUser=ProfileFactory.getInstance().getPrivateProfile(null);
            searchedUser = ProfileFactory.getInstance().getPublicProfile(null, currentUser.getOwner());
        } catch (InvalidObjectException | SQLException e) {
            e.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
        }
        //Set page factory for friends list pagination
        friendsListPagination.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer param) {
                try {
                    selectFriendsListPage(param);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                } catch (ServiceError serviceError) {
                    new Alert(Alert.AlertType.ERROR, serviceError.getMessage()).show();
                }
                return new Label("");
            }
        });
        friendsList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>()
                {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty || item==null) {
                            setStyle(null);
                            setText(null);
                            setGraphic(null);
                        }
                        else {
                            super.setStyle("-fx-background-color: #1e3475; -fx-text-fill: white");
                            super.setGraphic(new Label(item));
                            super.getGraphic().setStyle("-fx-background-color: #4776ff; -fx-text-fill:white");
                            super.setText("");
                        }
                    }
                };
            }
        });
        messagesList.setCellFactory(new Callback<ListView<MessageDTO>, ListCell<MessageDTO>>() {
            @Override
            public ListCell<MessageDTO> call(ListView<MessageDTO> param) {
                return new ListCell<MessageDTO>()
                {
                    @Override
                    protected void updateItem(MessageDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty || item==null)
                        {
                            setText(null);
                            setGraphic(null);
                            setStyle(null);
                        }
                        else
                        {
                            String fill ="";
                            if(item.getReplyTo()==null)
                            {
                                fill=String.format("%s sent at %s: %s",
                                        item.getSender().getNickname(),
                                        item.getDatetime().format(dateTimeFormatter),
                                        item.getMessage());
                                setStyle("-fx-background-color: #e6e6ff;-fx-font-size: 16px;-fx-font-weight: bold");
                            }
                            else
                            {
                                fill = String.format("%s replied to \"%s\" at %s: %s",
                                        item.getSender().getNickname(),
                                        item.getReplyTo().getMessage(),
                                        item.getDatetime().format(dateTimeFormatter),
                                        item.getMessage()
                                        );
                                setStyle("-fx-background-color: #3636fd;-fx-font-size: 16px;-fx-font-weight: bold;-fx-text-fill: white");
                            }
                            setText(fill);
                        }
                    }
                };
            }
        });
        receiversList.setCellFactory(new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                return new ListCell<String>()
                {
                    @Override
                    protected void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty || item==null)
                        {
                            setStyle(null);
                            setGraphic(null);
                            setText(null);
                        }
                        else
                        {
                            setStyle("-fx-font-size: 16px;-fx-text-fill: black");
                            setText(item);
                        }
                    }
                };
            }
        });
        //Set observers
        this.currentUser.addSubscriber(this);
        searchedUser.addSubscriber(this);
    }

    void updateFriendsList() throws SQLException, ServiceError {
        List<FriendshipDTO> friendshipDTOS = currentUser.getFriends();
        //Updating the page count
        friendsList.getItems().clear();
        int dim = friendshipDTOS.size();
        int computedPageCount = Integer.max((dim/pageSize)+((dim%pageSize!=0)?1:0),1);
        friendsListPagination.setPageCount(computedPageCount);
        friendsListPagination.setCurrentPageIndex(0);
        selectFriendsListPage(0);
    }

    private void selectFriendsListPage(int page) throws SQLException, ServiceError {
        //Clear friends list
        friendsList.getItems().clear();
        //Update the friends list with appropriate users
        if(currentUser.getOwner()!=null)
        {
            friendshipService.filterByUser(currentUser.getOwner().getId(), pageSize, page+1).forEach(f ->
            {
                User user;
                if (f.getSender().getId().equals(currentUser.getOwner().getId()))
                    user = f.getReceiver();
                else
                    user = f.getSender();
                String line = String.format("ID: %d,\n First Name: %s,\n Last Name: %s,\n Nickname: %s,\n Status: %s,\n Friends since: %s",
                        user.getId(),
                        user.getFirstName(),
                        user.getLastName(),
                        user.getNickname(),
                        (f.getStatus() == FriendshipStatus.PENDING ? "PENDING" : "ACCEPTED"),
                        f.getDatetime().format(dateFormat));
                friendsList.getItems().add(line);

            });
        }
    }

    void updateFriendshipToSearchedUser() throws SQLException {
        Friendship potentialFriendship = searchedUser.getFriendshipBetweenUsers();
        String friendshipStatus = "";
        if (potentialFriendship == null)
        {
            friendshipStatus = String.format("You and %s aren't friends!",searchedUser.getOwner().getNickname());
        }
        else if (!potentialFriendship.isAccepted()) {
            if (potentialFriendship.getId().getLeft().equals(searchedUser.getOwner().getId()))
                friendshipStatus = "%s has sent you a friend request!";
            else if (potentialFriendship.getId().getRight().equals(searchedUser.getOwner().getId()))
                friendshipStatus = "You sent %s a friend request!";
            friendshipStatus=String.format(friendshipStatus, searchedUser.getOwner().getNickname());
        } else
        {
            friendshipStatus = String.format("You and %s are friends since %s!",
                    searchedUser.getOwner().getNickname(),
                    searchedUser
                            .getFriendshipBetweenUsers()
                            .getDateTime().format(dateFormat));
        }
        friendshipStatusLabel.setText(friendshipStatus);
        friendshipStatusLabel.setVisible(true);
    }

    void updateFriendshipActionButton() {
        if (searchedUser.getOwner() == null) {
            friendshipActionButton.setVisible(false);
            friendshipActionButton.setText("Friendship action");
        } else {
            friendshipActionButton.setVisible(true);
            Friendship potentialFriendship = searchedUser.getFriendshipBetweenUsers();
            if (potentialFriendship == null)
                friendshipActionButton.setText("Send friendship request");
            else if (!potentialFriendship.isAccepted()) {
                if (potentialFriendship.getId().getLeft().equals(currentUser.getOwner().getId()))
                    friendshipActionButton.setText("Cancel friendship request");
                else if (potentialFriendship.getId().getRight().equals(currentUser.getOwner().getId()))
                    friendshipActionButton.setText("Reply friendship request");
            } else
                friendshipActionButton.setText("Delete friendship");
        }
    }

    public void onSearchUser() throws SQLException {
        try {
            String selectedNickname = nicknameSearchInput.getSelectionModel().getSelectedItem();
            if(selectedNickname!=null) {
                if (selectedNickname.isEmpty())
                    throw new UIError("No nickname is typed in!");
                List<User> searchedUserResults = userService.filterUsersNickname(selectedNickname);
                if (searchedUserResults.isEmpty())
                    throw new ServiceError("User doesn't exist!");
                if(selectedNickname.equals(currentUser.getOwner().getNickname()))
                    throw new UIError("You can't search yourself!");
                nicknameSearchInput.getItems().clear();
                searchedUserResults.forEach(u -> {
                    nicknameSearchInput.getItems().add(u.getNickname());
                });
                nicknameSearchInput.getSelectionModel().clearAndSelect(0);
                selectSearchedUser(selectedNickname);
            }
        } catch (ServiceError | UIError serviceError) {
            new Alert(Alert.AlertType.ERROR, serviceError.getMessage()).showAndWait();
        }
    }

    public void onFriendshipAction() {
        try {
            if (searchedUser == null || searchedUser.getOwner()==null)
                throw new UIError("Search someone first!");
            Friendship potentialFriendship = searchedUser.getFriendshipBetweenUsers();
            if (potentialFriendship == null) {
                friendshipService.sendFriendshipRequest(searchedUser.getLoggedUser().getId(), searchedUser.getOwner().getId());
                new Alert(Alert.AlertType.INFORMATION, "Friendship request sent!").show();
            } else if (potentialFriendship.getId().getLeft().equals(currentUser.getOwner().getId())) {
                friendshipService.removeFriendship(searchedUser.getLoggedUser().getId(), searchedUser.getOwner().getId());
                new Alert(Alert.AlertType.CONFIRMATION, "Friendship request canceled!").show();
            } else if (potentialFriendship.isAccepted()) {
                friendshipService.removeFriendship(searchedUser.getOwner().getId(), currentUser.getOwner().getId());
                new Alert(Alert.AlertType.CONFIRMATION, "Friendship deleted!");
            } else if (potentialFriendship.getId().getRight().equals(searchedUser.getLoggedUser().getId())) {
                Optional<ButtonType> reply = new Alert(Alert.AlertType.CONFIRMATION, String.format("Do you accept or reject %s's friendship?", searchedUser.getOwner().getNickname()), ButtonType.YES, ButtonType.NO).showAndWait();
                if (reply.isPresent())
                    if (reply.get() == ButtonType.NO) {
                        friendshipService.rejectFriendshipRequest(searchedUser.getOwner().getId(), searchedUser.getLoggedUser().getId());
                        new Alert(Alert.AlertType.CONFIRMATION, "Friendship request rejected!").show();
                    } else {
                        friendshipService.acceptFriendshipRequest(searchedUser.getOwner().getId(), currentUser.getOwner().getId());
                        new Alert(Alert.AlertType.CONFIRMATION, "Friendship request accepted!").show();
                    }
            }
        } catch (RepoError | DomainError | ServiceError | UIError repoError) {
            new Alert(Alert.AlertType.ERROR, repoError.getMessage()).show();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public void reinitialize() throws SQLException, ServiceError {
        reinitializeProfiles();
        reinitializeMainTabPane();
        reinitializeInputs();
        reinitializeButtons();
        reinitializeLabels();
        reinitializeLists();
    }

    private void reinitializeLists() {
        messagesList.getItems().clear();
        ownEventsTable.getItems().clear();
        friendsList.getItems().clear();
        friendsListPagination.setPageCount(1);
    }

    private void reinitializeLabels() {
        friendshipStatusLabel.setText("You and %s aren't friends!");
        friendshipStatusLabel.setVisible(false);
    }

    private void reinitializeButtons() {
        friendshipActionButton.setVisible(false);
        sendSearchedUserMessageButton.setVisible(false);
        checkNotificationsButton.setText("(0)");
    }

    private void reinitializeInputs() {
        nicknameSearchInput.getItems().clear();
        changeinfoCurrentPasswordInput.setText("");
        changeinfoNewPasswordInput.setText("");
        changeinfoNewNicknameInput.setText("");
        changeinfoNewFirstNameInput.setText("");
        changeinfoNewLastNameInput.setText("");
    }

    private void reinitializeMainTabPane() {
        mainTabPane.getSelectionModel().selectFirst();
    }

    private void reinitializeProfiles() throws SQLException, ServiceError {
        currentUser.setOwner(null);
        searchedUser.setOwner(null);
        searchedUser.setLoggedUser(null);
    }

    public void onLogoutRequest(ActionEvent actionEvent) {
        gui.setScene("Login");
        try {
            reinitialize();
            currentUser.getEventsHandler().stopEventChecks();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage());
        }
    }

    public void onSendSearchedUserMessage() {
        TextInputDialog messageDialog = new TextInputDialog();
        messageDialog.setTitle(String.format("Send %s a message", searchedUser.getOwner().getNickname()));
        messageDialog.setContentText("Send this user a message by typing below the message and then hit OK");
        Optional<String> messageText = messageDialog.showAndWait();
        if (messageText.isPresent()) {
            try {
                messageService.sendMessage(searchedUser.getLoggedUser().getId(), searchedUser.getOwner().getId(), messageText.get());
                new Alert(Alert.AlertType.INFORMATION, "Message sent successfully").show();
                updateChats();
            } catch (ServiceError | DomainError | RepoError serviceError) {
                new Alert(Alert.AlertType.ERROR, serviceError.getMessage()).show();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    public void onReceiversListClicked(MouseEvent mouseEvent) {
        if (receiversList.getSelectionModel().getSelectedIndices().size() == 1)
            showMessagesBetweenUsers(receiversList.getSelectionModel().getSelectedItem());
        else
            messagesList.getItems().clear();
    }

    private void showMessagesBetweenUsers(String selectedItem) {
        try {
            Optional<User> selectedUser = userService.getUserByNickname(selectedItem);
            if (selectedUser.isPresent()) {
                List<MessageDTO> messages = currentUser.getMessages().stream().filter(m->
                {
                    return m.getSender().getNickname().equals(selectedItem)|| m.getReceivers().stream().anyMatch(u->{return u.getNickname().equals(selectedItem);});
                }).collect(Collectors.toList());
                messagesList.getItems().clear();
                messages.forEach(messagesList.getItems()::add);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onSendMessageClicked(MouseEvent mouseEvent) {
        onSendMessage();
    }

    public void onSendMessage() {
        try {
            if (messageInput.getText().isEmpty())
                throw new UIError("Empty message!");
            List<Long> selectedReceivers = new ArrayList<>();
            List<String> selectedNicknames = receiversList.getSelectionModel().getSelectedItems();
            receiversList.getSelectionModel().getSelectedItems().forEach(n ->
            {
                try {
                    selectedReceivers.add(userService.getUserByNickname(n).get().getId());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            });
            messageService.sendMessage(currentUser.getOwner().getId(),selectedReceivers,messageInput.getText());
            selectedNicknames.forEach(n->{nicknameSearchInput.getSelectionModel().select(n);});
        } catch (UIError | RepoError | DomainError | ServiceError uiError) {
            new Alert(Alert.AlertType.ERROR,uiError.getMessage());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        messageInput.clear();
    }

    public void onReplyMessage(MouseEvent mouseEvent) {
        try {
            if (messagesList.getSelectionModel().getSelectedIndex() == -1)
                return;
            String selectedNickname = receiversList.getSelectionModel().getSelectedItem();
            if(selectedNickname!=null) {
                Optional<User> selectedUser = userService.getUserByNickname(selectedNickname);
                if (selectedUser.isPresent()) {
                    MessageDTO potentialMessage = messageService.getMessagesBetweenUsers(currentUser.getOwner().getId(), selectedUser.get().getId()).get(messagesList.getSelectionModel().getSelectedIndex());
                    TextInputDialog textInputDialog = new TextInputDialog();
                    textInputDialog.setTitle("Reply to message");
                    textInputDialog.setContentText(String.format("Reply to %s's message (%s)", potentialMessage.getSender().getNickname(), potentialMessage.getMessage()));
                    Optional<String> replyMsg = textInputDialog.showAndWait();
                    if (replyMsg.isPresent()) {
                        messageService.replyToMessage(currentUser.getOwner().getId(), potentialMessage.getId(), replyMsg.get());
                    }
                }
                showMessagesBetweenUsers(selectedNickname);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (RepoError | ServiceError | DomainError repoError) {
            new Alert(Alert.AlertType.ERROR, repoError.getMessage()).show();
        }
    }

    public void onSearchButtonClicked(MouseEvent mouseEvent) throws SQLException {
        onSearchUser();
    }

    public void onSearchButtonKeyPressed(KeyEvent keyEvent) throws SQLException {
        if (keyEvent.getCode() == KeyCode.ENTER)
            onSearchUser();
    }

    private void selectSearchedUser(String selectedItem) throws SQLException {
        if(selectedItem!=null&&!selectedItem.isEmpty()) {
            Optional<User> potentialUser = userService.getUserByNickname(selectedItem);
            try {
                if (!potentialUser.isPresent())
                    throw new ServiceError("User doesn't exist!");
                searchedUser.setOwner(potentialUser.get());
                searchedUser.notifyObservers();
            } catch (ServiceError serviceError) {
                new Alert(Alert.AlertType.ERROR, serviceError.getMessage());
            }
        }
    }

    public void onSearchedNicknameSelect(ActionEvent actionEvent) throws SQLException {
        String input = nicknameSearchInput.getValue();
        selectSearchedUser(input);
    }

    public void onSearchedNicknameKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.DOWN)
            nicknameSearchInput.getSelectionModel().selectNext();
        else if (keyEvent.getCode() == KeyCode.UP)
            nicknameSearchInput.getSelectionModel().selectPrevious();
    }

    public void onSendMessageButtonKeyPressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER)
            onSendMessage();
    }

    /**
     * updates the status of this Observer
     */
    @Override
    public void update() throws SQLException, ServiceError {
        updateProfile();
        updateFriendsList();
        updateSearchedUserProfile();
        updateChats();
        if(!receiversList.getSelectionModel().isEmpty())
            showMessagesBetweenUsers(receiversList.getSelectionModel().getSelectedItem());
        updateEventsNotifications();
        updateOwnEvents();
        updateAllEvents();
        updateUserSettings();
    }

    private void updateUserSettings() {
        eventsNotificationsCheckBox.setSelected(currentUser.getOwner().getUserSettings().isReceiveEventsNotifications());
    }

    private void updateAllEvents() {
        allEventsTable.getItems().clear();
        try {
            for(Event e:eventService.getAllEvents())
            {
                allEventsTable.getItems().add(new EventsTableEntry(new SimpleStringProperty(e.getId().toString()), new SimpleStringProperty(e.getTitle())));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void updateOwnEvents() throws SQLException, ServiceError {
        ownEventsTable.getItems().clear();
        for(Event e:eventService.getEventsUserCreated(currentUser.getOwner().getId()))
        {
            ownEventsTable.getItems().add(new EventsTableEntry(new SimpleStringProperty(e.getId().toString()), new SimpleStringProperty(e.getTitle())));
        }
    }

    private void updateEventsNotifications() {
        if(currentUser.getOwner().getUserSettings().isReceiveEventsNotifications())
        {
            updateNotificationsButton();
        }
    }

    private void updateNotificationsList() {
        while(!currentUser.getEventsHandler().getUpcomingEvents().isEmpty())
        {
            Pair<Event,EventNotifyTime> ev = currentUser.getEventsHandler().getUpcomingEvents().poll();
            String announceMessage="There are less than %d %s remaining until event %s starts!";
            String timeUnit="";
            assert ev != null;
            switch(ev.getValue().getUnit())
            {
                case DAYS:
                {
                    timeUnit="days";
                    break;
                }
                case HOURS:
                {
                    timeUnit="hours";
                    break;
                }
                case MINUTES:
                {
                    timeUnit="minutes";
                    break;
                }
                default:
                    timeUnit="";
            }
            //notificationsList.getItems().add(String.format(announceMessage,ev.getValue().getTimeUnits(),timeUnit,ev.getKey().getTitle()));
        }
    }

    private void updateNotificationsButton() {
        checkNotificationsButton.setText(String.format("(%d)",currentUser.getEventsHandler().getUpcomingEvents().size()));
        if(currentUser.getEventsHandler().getUpcomingEvents().size()>0)
            checkNotificationsButton.setStyle("-fx-background-color: green");
        else
            checkNotificationsButton.setStyle(null);
    }

    private void updateSearchedUserProfile() throws SQLException {
        if(searchedUser.getOwner()!=null)
        {
            updateSearchedUserName();
            updateFriendshipToSearchedUser();
            updateFriendshipActionButton();
            sendSearchedUserMessageButton.setVisible(true);
        }
    }

    private void updateSearchedUserName() {
        searchedUserNameLabel.setText(String.format("%s %s (%s)",
                searchedUser.getOwner().getFirstName(),
                searchedUser.getOwner().getLastName(),
                searchedUser.getOwner().getNickname()));
    }

    public PrivateProfile getCurrentUser() {
        return currentUser;
    }

    public void onReceiversListKeyPressed(KeyEvent keyEvent) {
        if (receiversList.getSelectionModel().getSelectedIndices().size() == 1)
            showMessagesBetweenUsers(receiversList.getSelectionModel().getSelectedItem());
        else
            messagesList.getItems().clear();
    }

    public void onFriendshipActionKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onFriendshipAction();
    }

    public void onFriendshipActionClicked(MouseEvent mouseEvent) {
        onFriendshipAction();
    }

    public void onSendSearchedUserMessageKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onSendSearchedUserMessage();
    }

    public void onSendSearchedUserMessageClicked(MouseEvent mouseEvent) {
        onSendSearchedUserMessage();
    }

    public void onChangeInfoRequest()
    {
        try
        {
            Optional<ButtonType> confirmation = new Alert(Alert.AlertType.CONFIRMATION,"Are you sure?",ButtonType.YES,ButtonType.NO).showAndWait();
            if(confirmation.isPresent()&&confirmation.get()==ButtonType.YES) {
                if (!currentUser.getOwner().getPasswd().equals(changeinfoCurrentPasswordInput.getText()))
                    throw new ServiceError("Incorrect current password!");
                userService.setUserSettings(currentUser.getOwner().getId(), eventsNotificationsCheckBox.isSelected());
                if (!changeinfoNewPasswordInput.getText().isEmpty())
                    userService.setPassword(currentUser.getOwner().getId(), changeinfoNewPasswordInput.getText());
                userService.setUserName(currentUser.getOwner().getId(), changeinfoNewFirstNameInput.getText(), changeinfoNewLastNameInput.getText());
                if (!changeinfoNewNicknameInput.getText().isEmpty())
                    userService.setNickname(currentUser.getOwner().getId(), changeinfoNewNicknameInput.getText());
                if(!changeinfoNewEmail.getText().isEmpty())
                    userService.setUserEmail(currentUser.getOwner().getId(),changeinfoNewEmail.getText());
                new Alert(Alert.AlertType.INFORMATION,"Your info has been updated!").show();
                //Clear input
                changeinfoCurrentPasswordInput.setText("");
                changeinfoNewPasswordInput.setText("");
                changeinfoNewNicknameInput.setText("");
                changeinfoNewFirstNameInput.setText("");
                changeinfoNewLastNameInput.setText("");
            }
        } catch (ServiceError|RepoError | DomainError  serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onSaveInfoButtonClicked(MouseEvent mouseEvent) {
        onChangeInfoRequest();
    }

    public void onSaveInfoButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onChangeInfoRequest();
    }

    void onActivityReportRequest()
    {
        try
        {
            gui.getActivityController().setCurrentUser(currentUser);
            if(searchedUser!=null)
                gui.getActivityController().setOtherUser(searchedUser);
            else
                throw new UIError("Search an user first!");
            currentUser.addSubscriber(gui.getActivityController());
            searchedUser.addSubscriber(gui.getActivityController());
            gui.setScene("Activities");
            gui.getActivityController().update();
        } catch (UIError|ServiceError uiError) {
            new Alert(Alert.AlertType.ERROR,uiError.getMessage()).show();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onActivityRequestClicked(MouseEvent mouseEvent) {
        onActivityReportRequest();
    }

    public void onActivityRequestKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onActivityReportRequest();
    }

    public void onCreateEventKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onCreateEventRequested();
    }

    public void onCreateEventClicked(MouseEvent mouseEvent) {
        onCreateEventRequested();
    }

    private void onCreateEventRequested() {
        try
        {
            if(eventTitleInput.getText().isEmpty())
                throw new UIError("Title can't be empty!");
            if(eventDescriptionInput.getText().isEmpty())
                throw new UIError("Description can't be empty!");
            if(eventStartDatePicker.getValue()==null)
                throw new UIError("Please select a start date!");
            if(eventStartTimePicker.getText().isEmpty())
                throw new UIError("Please enter a start time!");
            Pattern[] timePatterns = new Pattern[]
                    {
                            Pattern.compile("^[0-1]?[0-9]:[0-5][0-9]$"),
                            Pattern.compile("^[2][0-3]:[0-5][0-9]$")
                    };
            if(Arrays.stream(timePatterns).noneMatch(p->
            {
                return p.matcher(eventStartTimePicker.getText()).matches();
            }))
                throw new UIError("Invalid start time!");
            if(eventEndDatePicker.getValue()==null)
                throw new UIError("Please select an end date!");
            if(eventEndTimePicker.getText().isEmpty())
                throw new UIError("Please select an end time!");
            if(Arrays.stream(timePatterns).noneMatch(p->
            {
                return p.matcher(eventEndTimePicker.getText()).matches();
            }))
                throw new UIError("Invalid end time!");
            class TimeFromString
            {
                private int getPart(String time,int part)
                {
                    return Integer.parseInt(time.split(":")[part]);
                }
                public int getHour(String time)
                {
                    return getPart(time,0);
                }
                public int getMinutes(String time)
                {
                    return getPart(time,1);
                }
            }
            TimeFromString timeFromString = new TimeFromString();
            LocalDateTime startDate = eventStartDatePicker.getValue().atTime(timeFromString.getHour(eventStartTimePicker.getText()),timeFromString.getMinutes(eventStartTimePicker.getText()));
            LocalDateTime endDate = eventEndDatePicker.getValue().atTime(timeFromString.getHour(eventEndTimePicker.getText()),timeFromString.getMinutes(eventEndTimePicker.getText()));
            eventService.createEvent(currentUser.getOwner().getId(),
                    eventTitleInput.getText(),
                    eventDescriptionInput.getText(),
                    startDate,
                    endDate
                    );
            new Alert(Alert.AlertType.INFORMATION,"Event created successfully!").show();
        } catch (DomainError|UIError|ServiceError|RepoError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onRemoveEventKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onRemoveEventRequested();
    }

    private void onRemoveEventRequested() {
        try
        {
            if(ownEventsTable.getSelectionModel().getSelectedItem()==null)
                throw new UIError("No event selected!");
            eventService.removeEvent(Long.parseLong(ownEventsTable.getSelectionModel().getSelectedItem().getEventId()));
        } catch (UIError | RepoError | ServiceError uiError) {
            new Alert(Alert.AlertType.ERROR,uiError.getMessage()).show();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onRemoveEventClicked(MouseEvent mouseEvent) {
        onRemoveEventRequested();
    }

    public void onJoinButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onJoinEventRequest();
    }

    private void onJoinEventRequest() {
        try
        {
            if(allEventsTable.getSelectionModel().getSelectedItem()==null)
                throw new UIError("Select an event to join first!");
            eventService.attendEvent(Long.valueOf(allEventsTable.getSelectionModel().getSelectedItem().getEventId()),currentUser.getOwner().getId());
            new Alert(Alert.AlertType.INFORMATION,"You have joined this event successfully!").show();
        } catch (RepoError | DomainError | ServiceError|UIError uiError) {
            new Alert(Alert.AlertType.ERROR, uiError.getMessage()).show();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onUnjoinButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onUnjoinEventRequest();
    }

    private void onUnjoinEventRequest() {
        try
        {
            if(allEventsTable.getSelectionModel().getSelectedItem()==null)
                throw new UIError("Select an event first!");
            eventService.removeAttendance(Long.valueOf(allEventsTable.getSelectionModel().getSelectedItem().getEventId()),currentUser.getOwner().getId());
            new Alert(Alert.AlertType.INFORMATION,"You have removed your attendance from this event successfully!").show();
        } catch (RepoError | DomainError | ServiceError|UIError uiError) {
            new Alert(Alert.AlertType.ERROR, uiError.getMessage()).show();
        }catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onJoinButtonClicked(MouseEvent mouseEvent) {
        onJoinEventRequest();
    }

    public void onUnjoinButtonClicked(MouseEvent mouseEvent) {
        onUnjoinEventRequest();
    }

    public void onNotificationsButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            showNotifications();
    }

    private void showNotifications() {
        VBox vBox = new VBox(5);
        ListView<Pair<Event,EventNotifyTime>> notificationsList = new ListView<>();
        notificationsList.setCellFactory(new Callback<ListView<Pair<Event, EventNotifyTime>>, ListCell<Pair<Event, EventNotifyTime>>>() {
            @Override
            public ListCell<Pair<Event, EventNotifyTime>> call(ListView<Pair<Event, EventNotifyTime>> param) {
                return new ListCell<Pair<Event,EventNotifyTime>>()
                {
                    @Override
                    protected void updateItem(Pair<Event, EventNotifyTime> item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty||item==null)
                        {
                            setStyle(null);
                            setText(null);
                            setGraphic(null);
                        }
                        else
                        {
                            setText(String.format("There are less than %d %s until event %s starts.",
                                    item.getValue().getTimeUnits(),
                                    item.getValue().getUnit().toString(),
                                    item.getKey().getTitle()));
                            setStyle("-fx-font-family: 'Courier New';-fx-font-size: 16px;-fx-font-weight: bold;-fx-background-color: #272688;-fx-text-fill: white");
                        }
                    }
                };
            }
        });
        Scene scene = new Scene(vBox);
        Label title = new Label("Notifications");
        title.setStyle("-fx-font-size: 32px;-fx-font-family: 'Times New Roman';-fx-text-fill: blue;-fx-font-weight: bold");
        vBox.setAlignment(Pos.TOP_CENTER);
        vBox.getChildren().add(title);
        vBox.getChildren().add(notificationsList);
        Stage stage = new Stage();
        while(!currentUser.getEventsHandler().getUpcomingEvents().isEmpty())
        {
            notificationsList.getItems().add(currentUser.getEventsHandler().getUpcomingEvents().poll());
        }
        stage.setScene(scene);
        stage.show();
        updateNotificationsButton();
    }

    public void onNotificationsButtonClicked(MouseEvent mouseEvent) {
        showNotifications();
    }

    public void onNotificationKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()!=KeyCode.TAB)
            onReadNotification();
    }

    private void onReadNotification() {
    }

    public void onNotificationClicked(MouseEvent mouseEvent) {
        onReadNotification();
    }
    public void onOwnEventKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER) {
            try {
                showEventPopup(eventService.getEvent(Long.parseLong(ownEventsTable.getSelectionModel().getSelectedItem().eventId.toString())));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ServiceError serviceError) {
                new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
            }
        }
    }

    private void showEventPopup(Event event) {
        Scene scene;
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("/views/eventsView.fxml"));
        VBox vBox;
        try {
            vBox=fxmlLoader.load();
            EventsPopupController controller = fxmlLoader.getController();
            controller.setEvent(event);
            controller.setUserService(userService);
            scene = new Scene(vBox);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onOwnEventClicked(MouseEvent mouseEvent) {
        try {
            showEventPopup(eventService.getEvent(Long.parseLong(ownEventsTable.getSelectionModel().getSelectedItem().eventId.get())));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
        }
    }

    public void onEventsExplorerKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER) {
            try {
                showEventPopup(eventService.getEvent(Long.parseLong(allEventsTable.getSelectionModel().getSelectedItem().eventId.get())));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ServiceError serviceError) {
                new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
            }
        }
    }

    public void onEventsExplorerClicked(MouseEvent mouseEvent) {
        try {
            showEventPopup(eventService.getEvent(Long.parseLong(allEventsTable.getSelectionModel().getSelectedItem().eventId.get())));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
        }
    }
}
