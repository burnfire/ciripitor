package UI.GUI.Controlers;

import Domain.Errors.DomainError;
import Domain.Errors.RepoError;
import Domain.Errors.ServiceError;
import Domain.Errors.UIError;
import Services.UserService;
import UI.GUI.GUI;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import java.sql.SQLException;
import java.util.Optional;

public class RegisterController {

    UserService userService;
    GUI gui;

    @FXML
    TextField firstNameInput;
    @FXML
    TextField lastNameInput;
    @FXML
    TextField nicknameInput;
    @FXML
    PasswordField passwordInput;
    @FXML
    PasswordField confirmPasswordInput;
    @FXML
    TextField emailInput;
    public void setGui(GUI gui) {
        this.gui = gui;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void onLoginRequested() {
        gui.setScene("Login");
    }

    public void onRegister() {
        String firstName,lastName,nickname,password,email;
        firstName=firstNameInput.getText();
        lastName=lastNameInput.getText();
        nickname=nicknameInput.getText();
        password=passwordInput.getText();
        email = emailInput.getText();
        try {
            Alert confirmation = new Alert(Alert.AlertType.CONFIRMATION,"Are you sure you want to register?", ButtonType.YES,ButtonType.NO);
            Optional<ButtonType> response = confirmation.showAndWait();
            if(response.isPresent() && response.get()==ButtonType.YES) {
                if(!passwordInput.getText().equals(confirmPasswordInput.getText()))
                    throw new UIError("Passwords don't match!");
                userService.register(firstName,lastName,nickname,password,email);
                new Alert(Alert.AlertType.INFORMATION,String.format("Welcome, %s!",nickname)).showAndWait();
                gui.setScene("Login");
            }
        } catch (RepoError | ServiceError | DomainError | UIError repoError) {
            new Alert(Alert.AlertType.ERROR,repoError.getMessage()).showAndWait();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public void onRegisterButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()== KeyCode.ENTER)
            onRegister();
    }

    public void onRegisterButtonClicked(MouseEvent mouseEvent) {
        onRegister();
    }

    public void onLoginRequestKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onLoginRequested();
    }

    public void onLoginRequestClicked(MouseEvent mouseEvent) {
        onLoginRequested();
    }
}
