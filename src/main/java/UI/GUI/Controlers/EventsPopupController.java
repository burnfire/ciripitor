package UI.GUI.Controlers;

import Domain.Errors.ServiceError;
import Domain.Events.Event;
import Domain.User;
import Services.UserService;
import UI.DateTimeFormats;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EventsPopupController implements DateTimeFormats {

    Event event;
    UserService userService;

    //FXML Objects
    @FXML
    Label eventNameLabel;
    @FXML
    TextArea descriptionTextArea;
    @FXML
    Label creatorNameLabel;
    @FXML
    Label startTimeLabel;
    @FXML
    Label endTimeLabel;
    @FXML
    ListView<User> participantsList;

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
        setListFactories();
        updateFields();
    }

    private void setListFactories() {
        setParticipantsListFactory();
    }

    private void setParticipantsListFactory() {
        participantsList.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {
            @Override
            public ListCell<User> call(ListView<User> param) {
                return new ListCell<User>()
                {
                    @Override
                    protected void updateItem(User item, boolean empty) {
                        super.updateItem(item, empty);
                        if(empty||item==null)
                        {
                            setStyle(null);
                            setGraphic(null);
                            setText(null);
                        }
                        else
                        {
                            setText(item.getNickname());
                            setStyle("-fx-font-size: 16px; -fx-font-family: 'Courier New'; -fx-font-weight: bold; -fx-background-color: #5e6a8c; -fx-text-fill: white; -fx-border-color: #000000");
                        }
                    }
                };
            }
        });
    }

    private void updateFields() {
        eventNameLabel.setText(event.getTitle());
        descriptionTextArea.setText(event.getDescription());
        try {
            creatorNameLabel.setText(String.format("Host: %s",userService.getUserById(event.getCreatorId()).getNickname()));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage());
        }
        startTimeLabel.setText(event.getStartDate().format(dateTimeFormatter));
        endTimeLabel.setText(event.getEndDate().format(dateTimeFormatter));
        participantsList.getItems().clear();
        List<User> list = new ArrayList<>();
        for (Long aLong : event.getAttendees()) {
            User userById = null;
            try {
                userById = userService.getUserById(aLong);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            } catch (ServiceError serviceError) {
                new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).show();
            }
            list.add(userById);
        }
        participantsList.getItems().addAll(list);
    }
}
