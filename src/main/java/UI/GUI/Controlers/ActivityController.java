package UI.GUI.Controlers;

import Domain.Errors.ServiceError;
import Domain.Errors.UIError;
import Domain.Observer.Observer;
import Domain.Profiles.PrivateProfile;
import Domain.Profiles.PublicProfile;
import Services.ActivityService;
import Services.DTOs.ActivityDTO;
import UI.GUI.GUI;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.PngImage;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;

import java.io.*;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ActivityController implements Observer {
    GUI gui;

    //Services
    ActivityService activityService;


    //Local classes
    enum ActivityType
    {
        FRIENDSHIPS,MESSAGES,ALL
    }
    public class ActivityTableEntry
    {
        private SimpleStringProperty description;
        private SimpleStringProperty dateTime;
        ActivityTableEntry(String description,LocalDateTime dateTime)
        {
            this.description=new SimpleStringProperty(description);
            this.dateTime=new SimpleStringProperty(dateTime.toString());
        }

        public String getDescription() {
            return description.get();
        }

        public SimpleStringProperty descriptionProperty() {
            return description;
        }

        public void setDescription(String description) {
            this.description.set(description);
        }

        public String getDateTime() {
            return dateTime.get();
        }

        public SimpleStringProperty dateTimeProperty() {
            return dateTime;
        }

        public void setDateTime(String dateTime) {
            this.dateTime.set(dateTime);
        }
    }
    public static final class ActivityPDFReport
    {
        //Make the PDF document stylish
        static void documentDesign(Document document,PrivateProfile loggedInUserProfile) throws IOException, DocumentException {
            //Add our mascot in here
            Image chirpy = PngImage.getImage(ActivityPDFReport.class.getResource("/views/images/Chirpy.png"));
            chirpy.scaleAbsolute(75,75);
            document.add(chirpy);
            //Add social network's name
            Chunk header = new Chunk("Ciripitor");
            Font headerFont = FontFactory.getFont("Times New Roman",32,new BaseColor(0,0,1,0.5f));
            header.setFont(headerFont);
            Paragraph headerParagraph = new Paragraph();
            headerParagraph.add(header);
            headerParagraph.setAlignment(Element.ALIGN_CENTER);
            document.add(headerParagraph);
            //Add the document's title
            Chunk title = new Chunk(String.format("%s's activity report",loggedInUserProfile.getOwner().getNickname()));
            Font titleFont = new Font();
            titleFont.setColor(0,0,0);
            titleFont.setSize(18);
            titleFont.setFamily("Times New Roman");
            title.setFont(titleFont);
            Paragraph paragraph = new Paragraph();
            paragraph.add(title);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);
        }

        static void documentProperties(Document document)
        {
            document.addTitle("Ciripitor activity report");
            document.addCreator("Ciripitor");
            document.addAuthor("Stefanescu Petru (burnfire)");
            document.addLanguage("English");
        }

        public static void exportPDFReport(String path,List<ActivityDTO> activities,PrivateProfile loggedInUserProfile) throws IOException, DocumentException {
            Document document = new Document();
            document.setPageSize(PageSize.A4);
            PdfWriter pdfWriter = PdfWriter.getInstance(document,new FileOutputStream(path));
            document.open();
            document.add(new Chunk(""));
            documentDesign(document,loggedInUserProfile);
            writeActivities(document,activities);
            documentProperties(document);
            document.close();
        }

        private static void writeActivities(Document document, List<ActivityDTO> activities) throws DocumentException {
            //Add some space before the table
            Paragraph p = new Paragraph();
            p.add(new Chunk("\n"));
            document.add(p);
            PdfPTable table = new PdfPTable(2);
            PdfPCell descriptionCell = new PdfPCell(new Phrase("Description"));
            PdfPCell dateCell = new PdfPCell(new Phrase("Date"));
            table.addCell(descriptionCell);
            table.addCell(dateCell);
            activities.forEach(a->
            {
                table.addCell(new PdfPCell(new Phrase(a.getDescription())));
                table.addCell(new PdfPCell(new Phrase(a.getDateTime().toString())));
            });
            document.add(table);
        }
    }

    @FXML
    ComboBox<ActivityType> activityTypeComboBox;
    @FXML
    DatePicker startDatePicker;
    @FXML
    DatePicker endDatePicker;
    @FXML
    Button getActivitiesButton;
    @FXML
    Label otherUserLabel;
    @FXML
    TableView<ActivityTableEntry> activitiesTable;

    //Profiles
    PrivateProfile currentUser;
    PublicProfile otherUser;

    public GUI getGui() {
        return gui;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
        //Config
        //Adding values to activity type combobox
        activityTypeComboBox.getItems().clear();
        activityTypeComboBox.getItems().add(ActivityType.FRIENDSHIPS);
        activityTypeComboBox.getItems().add(ActivityType.MESSAGES);
        activityTypeComboBox.getItems().add(ActivityType.ALL);
        //Config filtered activities table
        activitiesTable.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("description"));
        activitiesTable.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("dateTime"));
        //Defaults
        //Set default dates
        startDatePicker.setValue(LocalDate.now());
        endDatePicker.setValue(LocalDate.now());
        //Set default activity type
        activityTypeComboBox.getSelectionModel().selectLast();
    }

    public void setCurrentUser(PrivateProfile currentUser) {
        this.currentUser = currentUser;
    }

    public void setOtherUser(PublicProfile otherUser) {
        this.otherUser = otherUser;
        activityService=new ActivityService(currentUser,otherUser);
    }

    public void onBackButtonKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()== KeyCode.ENTER)
            onGoBackRequest();
    }

    public void onBackButtonClicked(MouseEvent mouseEvent) {
        onGoBackRequest();
    }

    public void onGoBackRequest()
    {
        gui.setScene("Home");
    }

    /**
     * updates the status of this Observer
     */
    @Override
    public void update() throws SQLException, ServiceError {
        if(otherUser.getOwner()==null)
        {
            otherUserLabel.setText("You didn't select any user. If you want to see activities with other user, go back and select him");
            otherUserLabel.setTextFill(new Color(1,0,0,1));
        }
        else
        {
            otherUserLabel.setText(String.format("You selected user %s. You will see activities between you two!",otherUser.getOwner().getNickname()));
            otherUserLabel.setTextFill(new Color(0,0,0,1));
        }

    }


    public void onGetActivitiesClicked(MouseEvent mouseEvent) {
        onShowActivities();
    }

    public void onGetActivitiesKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onShowActivities();
    }

    List<ActivityDTO> getActivities() throws UIError {
        if (startDatePicker.getValue() == null || endDatePicker.getValue() == null)
            throw new UIError("You didn't select both dates!");
        LocalDateTime startDate = startDatePicker.getValue().atStartOfDay();
        LocalDateTime endDate = endDatePicker.getValue().atTime(23, 59, 59);
        if(startDate.compareTo(endDate)>0)
            throw new UIError("Start date has to be before end date!");
        List<ActivityDTO> activities=new ArrayList<>();
        if (activityTypeComboBox.getSelectionModel().getSelectedItem() == ActivityType.FRIENDSHIPS) {
            activities.addAll(activityService.getFriendships(startDate,endDate));
        }
        else if (activityTypeComboBox.getSelectionModel().getSelectedItem() == ActivityType.MESSAGES) {
            activities.addAll(activityService.getMessages(startDate,endDate));
        }
        else if(activityTypeComboBox.getSelectionModel().getSelectedItem()== ActivityType.ALL)
        {
            activities.addAll(activityService.getFriendships(startDate,endDate));
            activities.addAll(activityService.getMessages(startDate,endDate));
        }
        else
            throw new UIError("Choose an activity type to filter");
        return activities;
    }

    private void onShowActivities() {
        try {
            activitiesTable.getItems().clear();
            List<ActivityDTO> activities=getActivities();
            for(ActivityDTO a:activities)
            {
                activitiesTable.getItems().add(new ActivityTableEntry(
                        a.getDescription(),
                        a.getDateTime()
                ));
            }
        } catch (UIError uiError) {
            new Alert(Alert.AlertType.ERROR,uiError.getMessage()).show();
        }
    }

    public void onExportPDFRequest()
    {
        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showSaveDialog(null);
        try
        {
            ActivityPDFReport.exportPDFReport(file.getAbsolutePath()+".pdf",getActivities(),currentUser);
            new Alert(Alert.AlertType.INFORMATION,"PDF document created successfully!").show();
        }
        catch(UIError uiError)
        {
            new Alert(Alert.AlertType.ERROR,uiError.getMessage()).show();
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    public void onExportPDFClicked(MouseEvent mouseEvent) {
        onExportPDFRequest();
    }

    public void onExportPDFKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onExportPDFRequest();
    }
}
