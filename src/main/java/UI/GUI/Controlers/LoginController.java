package UI.GUI.Controlers;

import Domain.Errors.ServiceError;
import Domain.User;
import Services.UserService;
import UI.GUI.GUI;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.InvalidObjectException;
import java.sql.SQLException;

public class LoginController {
    @FXML
    TextField nicknameInput;
    @FXML
    PasswordField passwordInput;
    @FXML
    VBox vBoxPrincipal;
    @FXML
    AnchorPane loginAnchor;
    @FXML
    Button loginButton;
    @FXML
    Hyperlink registerLink;

    User loggedIn;
    UserService userService;
    GUI gui;
    HomeController homeController;

    public void setHomeController(HomeController homeController) {
        this.homeController = homeController;
    }

    public void setGui(GUI gui) {
        this.gui = gui;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public LoginController() {

    }

    public void onLogin() {
        String nickname = nicknameInput.getText();
        String password = passwordInput.getText();
        try {
            loggedIn = userService.login(nickname,password);
            new Alert(Alert.AlertType.INFORMATION,String.format("Welcome back, %s!",loggedIn.getNickname())).showAndWait();
            gui.setScene("Home");
            homeController.setCurrentUser(loggedIn);
        } catch (SQLException | InvalidObjectException throwables) {
            throwables.printStackTrace();
        } catch (ServiceError serviceError) {
            new Alert(Alert.AlertType.ERROR,serviceError.getMessage()).showAndWait();
        }
        nicknameInput.clear();
        passwordInput.clear();
    }

    public void onRegisterRequest() {
        gui.setScene("Register");
    }

    public void onLoginButtonClicked(MouseEvent mouseEvent) {
        onLogin();
    }

    public void onLoginKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onLogin();
    }

    public void onRegisterRequestKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode()==KeyCode.ENTER)
            onRegisterRequest();
    }

    public void onRegisterRequestClicked(MouseEvent mouseEvent) {
        onRegisterRequest();
    }
}
